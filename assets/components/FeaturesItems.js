import React, {Component} from 'react';
import {getFeatureItems} from "../api/FeaturesItems";
import PropTypes from "prop-types";
import Pagination from "react-js-pagination";
import {getPriceRangeInput} from "../functions/functions";
import {Circles, ColorRing, Puff, Rings} from "react-loader-spinner";

class FeaturesItems extends Component {

    constructor(props) {
        super(props);

        this.addToCart = 'Додати до кошика';

        this.state = {
            items: [],
            paginatorData: [],
            showPagination: false,
            loading: false
        }

        const {randomOffers, pagination, categoryUrl} = props;
        const pageNumber = pagination ? 1 : 0;

        getFeatureItems(randomOffers, pageNumber, categoryUrl).then(data => {
            this.setState({
                items: data.items,
                paginatorData: data.paginatorData,
                showPagination: true,
                loading: true
            })

            const {setMinMaxPriceRange} = this.props

            if (setMinMaxPriceRange !== undefined) {
                setMinMaxPriceRange(data.priceRange)
            }
        })

        this.handlePageChange = this.handlePageChange.bind(this);
        this.redirectToProduct = this.redirectToProduct.bind(this);
        this.waitLoadImage = this.waitLoadImage.bind(this);
    }

    applyFilters(filters) {
        this.setState({
            loading: false
        })
        const {categoryUrl} = this.props;
        getFeatureItems(false, 1, categoryUrl, filters).then(data => {
            this.setState({
                items: data.items,
                paginatorData: data.paginatorData,
                showPagination: true,
                loading: true
            })
        })
    }

    handlePageChange(pageNumber) {
        this.setState({
            activePage: pageNumber,
            loading: false
        });

        const {categoryUrl} = this.props;

        let priceRange = getPriceRangeInput().slider().data().slider.value;
        let min = priceRange[0];
        let max = priceRange[1];
        let filters = {'priceRange': {min, max}};

        getFeatureItems(true, pageNumber, categoryUrl, filters).then(data => {
            this.setState({
                items: data.items,
                paginatorData: data.paginatorData,
                loading: true
            })
            window.scrollTo(0, 0);
        })

        dataLayer.push(
            {
                'event': 'pagination',
            }
        );
    }

    redirectToProduct(event, vendorCode) {
        if (event.target.tagName === "DIV" || event.target.tagName === "IMG") {
            window.location = "/product/" + vendorCode;
        }
    }

    waitLoadImage(event) {
        let img = event.target;
        let preload = event.target.nextElementSibling;

        img.style.display = 'block';
        preload.style.display = 'none';
    }


    render() {
        const {title, pagination, handleClick} = this.props;
        const {items, paginatorData, showPagination, loading} = this.state
        return (
            <>
                <div className="features_items">{/*features_items*/}
                    <h2 className="title text-center">{title}</h2>
                    {items.map((item, i) =>
                        <div className="col-sm-4" key={i}>
                            <div className="product-image-wrapper">
                                <div className="single-products">
                                    <div className="productinfo text-center " style={{cursor: 'pointer'}}
                                         onClick={(event) => this.redirectToProduct(event, item.url)}
                                    >
                                        {loading ?
                                            <>
                                                <img src={JSON.parse(item.offerPicture.pictures)[0]} alt={item.name}
                                                     title={item.name} onLoad={this.waitLoadImage}
                                                     style={{display: 'none'}}/>
                                                <div className={"loading"}>
                                                    <Circles
                                                        height="80"
                                                        width="80"
                                                        color="#FE980F"
                                                        ariaLabel="circles-loading"
                                                        wrapperStyle={{}}
                                                        disabled={false}
                                                    />
                                                </div>
                                                <h2 className="d-inline product-price">{item.priceMargin} {item.currencyName}</h2>
                                                {
                                                    (item.oldPrice !== undefined) ?
                                                        <h2 className="price-before-discount d-inline">{item.oldPrice} {item.currencyName}</h2>
                                                        : ''
                                                }
                                                <p>{item.name.split(' ').slice(0, 8).join(' ') + ' ...'}</p>
                                            </>
                                            :
                                            <div className={"loading"}>
                                                <Puff
                                                    height="80"
                                                    width="80"
                                                    color="#FE980F"
                                                    ariaLabel="circles-loading"
                                                    wrapperStyle={{}}
                                                    wrapperClass=""
                                                />
                                            </div>
                                        }
                                        <button className="btn btn-default add-to-cart"
                                                onClick={() => handleClick(item, 1)}>
                                            <i className="fa fa-shopping-cart"></i>{this.addToCart}
                                        </button>
                                    </div>


                                    {/*<div style={{cursor: 'pointer'}} className="product-overlay"*/}
                                    {/*     onClick={(event) => this.redirectToProduct(event, item.id)}>*/}
                                    {/*    <div className="overlay-content">*/}
                                    {/*        <h2>{item.price} {item.currencyName}</h2>*/}
                                    {/*        <p>{item.name}</p>*/}
                                    {/*        <button className="btn btn-default add-to-cart"*/}
                                    {/*                onClick={() => handleClick(item, 1)}>*/}
                                    {/*            <i className="fa fa-shopping-cart"></i>{this.addToCart}*/}
                                    {/*        </button>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}
                                </div>
                                {/*TODO will be implement in the future*/}
                                {/*<div className="choose">*/}
                                {/*    <ul className="nav nav-pills nav-justified">*/}
                                {/*        <li><a href="assets/pages/AppHome#"><i className="fa fa-plus-square"></i>Add*/}
                                {/*            to wishlist</a>*/}
                                {/*        </li>*/}
                                {/*        <li><a href="assets/pages/AppHome#"><i className="fa fa-plus-square"></i>Add*/}
                                {/*            to compare</a></li>*/}
                                {/*    </ul>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    )}
                </div>
                <div>
                    {pagination && showPagination && (
                        <Pagination
                            activePage={paginatorData.activePage}
                            itemsCountPerPage={paginatorData.itemsCountPerPage}
                            totalItemsCount={paginatorData.totalItemsCount}
                            pageRangeDisplayed={paginatorData.pageRangeDisplayed}
                            onChange={this.handlePageChange}
                        />
                    )}
                </div>
            </>
        );
    }
}

FeaturesItems.propTypes = {
    pagination: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    randomOffers: PropTypes.bool.isRequired,
    categoryUrl: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired
}

export default FeaturesItems;