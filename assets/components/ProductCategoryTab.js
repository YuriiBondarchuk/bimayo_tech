import React, {Component} from 'react';
import CurrentDateTime from "./CurrentDateTime";
import {sendMailComponent} from "../api/Mailer";
import QuestionForm from "./QuestionForm";

class ProductCategoryTab extends Component {
    constructor(props) {
        super(props);

        this.divRef = React.createRef();

        this.sendForm = this.sendForm.bind(this);
    }

    componentDidMount() {
        let {descriptions} = this.props;

        this.divRef.current.innerHTML = descriptions;
    }

    sendForm() {
        let {name, email, question, errorMessage, hasError} = this.state

        if (name.length === 0) {
            hasError.name = true;
        }
        if (email.length === 0) {
            hasError.name = true;
        }
        if (question.length === 0) {
            hasError.name = true;
        }

        if (Object.keys(hasError).includes(true)) {
            errorMessage = 'Це поле обов\'язкове та не може бути порожнім';
        } else {
            sendMailComponent(this.state)
        }

    }


    render() {
        let {params} = this.props;
        return (
            <div className="category-tab shop-details-tab">
                <div className="col-sm-12">
                    <ul className="nav nav-tabs">
                        <li className="active"><a href={"#details"} data-toggle="tab">Детально</a></li>
                        <li><a href={"#specification"} data-toggle="tab">Характеристики</a></li>
                        <li><a href={"#payment-delivery"} data-toggle="tab">Оплата та доставка</a></li>
                        <li><a href={"#question"} data-toggle="tab">Написати нам</a></li>
                    </ul>
                </div>
                <div className="tab-content">
                    <div className="tab-pane fade active in" id="details">
                        <CurrentDateTime/>
                        <h2>Детально</h2>
                        <div ref={this.divRef}></div>
                    </div>
                    <div className="tab-pane fade" id="specification">
                        <CurrentDateTime/>
                        <h2>Характеристики</h2>
                        <div className="specification-body">
                            {Object.keys(params).map((key, i) =>
                                <div className="specification-section" key={i}>
                                    <h4>{key.charAt(0).toUpperCase() + key.slice(1)}</h4>
                                    <div className="specification-line"></div>
                                    <span>{params[key].charAt(0).toUpperCase() + params[key].slice(1)}</span>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="tab-pane fade" id="payment-delivery">
                        <div className="col-sm-12">
                            <CurrentDateTime/>
                            <h2>Оплата та доставка</h2>
                            <div className="payment-delivery-body">
                                <div className="description-style">
                                    <div className="payment-delivery-information">
                                        <div className="p-item"><p><span
                                            style={{fontSize: '14pt'}}><strong>Способи доставки:</strong></span></p>
                                            <ul>
                                                <li>Транспортна компанія «Нова пошта»</li>
                                                <li>Транспортна компанія «Укрпошта»</li>
                                            </ul>
                                        </div>
                                        <div className="p-item"><p><span
                                            style={{fontSize: '14pt'}}><strong>Способи оплати:</strong></span></p>
                                            <ul>
                                                <li>Переказ на картку Visa та MasterCard</li>
                                                <li>Оплата після отримання ТК «Нова пошта»</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="question">
                        <CurrentDateTime/>
                        <QuestionForm/>
                    </div>
                </div>
            </div>);
    }
}

export default ProductCategoryTab;