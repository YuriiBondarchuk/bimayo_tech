import React, {Component} from 'react';
import PropTypes from "prop-types";

class Breadcrumbs extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {
        let {items} = this.props;

        return (
            <>
                <div className="breadcrumbs">
                    <ol className="breadcrumb">
                        {
                            items.map((item, key) => {
                                    const lastItem = items[items.length - 1];

                                    return lastItem !== item ?
                                        <li key={key.toString()} title={item.title}>

                                            <a href={item.url}>{item.title}
                                                <i className="fa fa-arrow-right"></i>
                                            </a>
                                        </li>
                                        :
                                        <li className="active" key={key.toString()} title={item.title}>
                                            {item.title.length > 30 ? item.title.substring(0, 30) + ' ...' : item.title}
                                        </li>
                                }
                            )
                        }
                    </ol>
                </div>
            </>
        )
            ;
    }
}

Breadcrumbs.propTypes = {
    items: PropTypes.array.isRequired
}

export default Breadcrumbs;


