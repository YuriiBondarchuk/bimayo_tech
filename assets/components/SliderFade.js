import ImageGallery from 'react-image-gallery';
import React, {Component} from 'react';
import PropTypes from "prop-types";

export default class SliderFade extends Component {
    constructor(props) {
        super(props);

        this.leftNav = React.createRef();
        this.righNav = React.createRef();

        this.prepareImagesForSlide = this.prepareImagesForSlide.bind(this);
        this.renderLeftNav = this.renderLeftNav.bind(this);
        this.renderRightNav = this.renderRightNav.bind(this);
        this.moveSliderNavigations = this.moveSliderNavigations.bind(this);
    }

    prepareImagesForSlide() {
        const {pictures, imageAlt} = this.props;
        let newImages = [];

        pictures.forEach((el) => {
            let image = [];
            image.original = el;
            image.thumbnail = el;
            image.originalAlt = imageAlt;
            image.thumbnailAlt = imageAlt;
            image.originalTitle = imageAlt;
            image.thumbnailTitle = imageAlt;

            newImages.push(image);
        })
        return newImages;
    }

    moveSliderNavigations() {
        let thumbnailsWrapper = document.getElementsByClassName('image-gallery-thumbnails-wrapper')[0];
        thumbnailsWrapper.append(this.leftNav.current ?? '')
        thumbnailsWrapper.append(this.righNav.current ?? '')
    }

    renderLeftNav(onClick) {
        return <a onClick={onClick} className={'left item-control'} data-slide={'prev'} id={"leftNav"}
                  ref={this.leftNav}
        >
            <i className={"fa fa-angle-left"}></i>
        </a>
    }

    renderRightNav(onClick) {
        return <a onClick={onClick} className={'right item-control'} data-slide={'next'} id={"rightNav"}
                  ref={this.righNav}>
            <i className={"fa fa-angle-right"}/>
        </a>
    }

    componentDidMount() {
        this.moveSliderNavigations();
    }

    render() {
        return <ImageGallery
            items={this.prepareImagesForSlide()}
            showPlayButton={false}
            showBullets={true}
            showIndex={true}
            renderLeftNav={this.renderLeftNav}
            renderRightNav={this.renderRightNav}
        />;
    }
}

SliderFade.propTypes = {
    pictures: PropTypes.array,
    imageAlt: PropTypes.string,
}