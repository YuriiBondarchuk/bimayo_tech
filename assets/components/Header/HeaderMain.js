import React, {Component} from 'react';
import HeaderTop from "./HeaderTop";
import HeaderMiddle from "./HeaderMiddle";
import HeaderBottom from "./HeaderBottom";
import {getHeaderData} from "../../api/Header";

class HeaderMain extends Component {
    constructor(props) {
        super(props);

        this.state = {
            headerInfo: [],
            headerMenu: []
        }

        getHeaderData().then(data => {
            this.setState({
                headerInfo: data.info,
                headerMenu: data.headerMenu
            })
        })
    }

    render() {
        const {handleClick,cartCountItems, minPriceRange, maxPriceRange, applyPriceRangeFilters, showFilters} = this.props;

        return (
            <header id="header"> {/*header-main*/}
                <HeaderTop
                    phone={this.state.headerInfo.phone}
                    email={this.state.headerInfo.email}

                /> {/*header-top*/}
                <HeaderMiddle handleClick={handleClick} cartCountItems={cartCountItems}/> {/*header-middle*/}
                <HeaderBottom
                    menu={this.state.headerMenu}
                    minPriceRange={minPriceRange} maxPriceRange={maxPriceRange}
                    applyPriceRangeFilters={applyPriceRangeFilters}
                    showFilters={showFilters}
                /> {/*header - bottom*/}
            </header>
        );
    }
}

export default HeaderMain;
