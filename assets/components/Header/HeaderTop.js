import React, {Component} from 'react';

class HeaderTop extends Component {
    render() {
        let phoneNumber = this.props.phone;
        let email = this.props.email;
        return (
            <>
                <div className="header_top">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="contactinfo">
                                    <ul className="nav nav-pills">
                                        <li>
                                            <a href="/contact"><i className="fa fa-phone"></i>
                                                {phoneNumber}
                                            </a>
                                        </li>
                                        <li><a href="/contact"><i
                                            className="fa fa-envelope"></i> {email}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="social-icons pull-right">
                                    <ul className="nav navbar-nav">
                                        <li><a href="https://www.instagram.com/bimayo.shop/" target={"_blank"}
                                               title={"Ми в Інстаграм"}><i className="fa fa-instagram fa-lg"></i></a>
                                        </li>
                                        <li><a href="https://www.facebook.com/shop.bimayo/shop" target={"_blank"}
                                               title={"Ми в Facebook"}><i className="fa fa-facebook fa-lg"></i></a></li>
                                        {/*<li><a href="assets/pages/AppHome#"><i className="fa fa-linkedin"></i></a></li>*/}
                                        {/*<li><a href="assets/pages/AppHome#"><i className="fa fa-dribbble"></i></a></li>*/}
                                        {/*<li><a href="assets/pages/AppHome#"><i className="fa fa-google-plus"></i></a></li>*/}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/*temporary: delete after run magazine in social; */}
                        {/*<div className="row">*/}
                        {/*    <div className="col-sm-6">*/}
                        {/*        <div className="social-icons pull-left">*/}
                        {/*            <ul className="nav nav-pills">*/}
                        {/*                <li><a href="/contact"><i className="fa fa-phone"> </i>*/}
                        {/*                    <span className={"ml-3"}>{phoneNumber}</span>*/}
                        {/*                </a>*/}
                        {/*                </li>*/}
                        {/*            </ul>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*    <div className="col-sm-6">*/}
                        {/*        <div className="social-icons pull-right">*/}
                        {/*            <ul className="nav navbar-nav">*/}
                        {/*                <li><a href="/contact"><i className="fa fa-envelope"></i>*/}
                        {/*                    {email}*/}
                        {/*                </a></li>*/}
                        {/*            </ul>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </>
        );
    }
}

export default HeaderTop;