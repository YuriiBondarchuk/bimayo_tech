import React, {Component} from 'react';
import Autocomplete from "../Autocomplete";
import CategoryProducts from "../CategoryProducts";
import PriceRange from "../PriceRange";
import {isMobile} from "../../functions/functions";

class HeaderBottom extends Component {
    render() {
        const headerMenu = this.props.menu
        const {minPriceRange, maxPriceRange, applyPriceRangeFilters, showFilters} = this.props;
        return (
            <div>
                <div className="header-bottom">
                    <div className="container">
                        <div className="row">
                            <div className="col-3 navbar_menu_output_div">
                                <div className="navbar-header main-menu">
                                    <a className="menu-trigger" href="#"></a>
                                    <div className="menu-popup">
                                        <a className="menu-close" href="#"></a>
                                        <ul className="">
                                            {
                                                headerMenu.map((item, key) => (
                                                    <li key={key}>
                                                        <a href={item.route} className={(item.active) ? "active" : ''}>
                                                            {item.name}
                                                        </a>
                                                    </li>
                                                ))
                                            }
                                        </ul>
                                        <div className="responsive-category-tree">
                                            {
                                                isMobile()
                                                ? <CategoryProducts numberAccordian={2}/>
                                                : <></>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-3 navbar_filters_output_div"
                                 style={{visibility: showFilters ? 'visible' : 'hidden'}}>
                                <div className="navbar-header main-filters">
                                    <a className="filters-trigger" href="#"></a>
                                    <div className="filters-popup">
                                        <a className="filters-close" href="#"></a>
                                        <h3>Фільтрувати:</h3>
                                        <PriceRange min={minPriceRange} max={maxPriceRange}
                                                    applyPriceRangeFilters={applyPriceRangeFilters}/>
                                    </div>
                                </div>
                            </div>
                            {/*search section */}
                            <Autocomplete countSuggestions={10}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeaderBottom;