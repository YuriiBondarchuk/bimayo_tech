import React, {Component} from 'react';
import {getCountItemsInCartFromStorage} from "../../functions/functions";

class HeaderMiddle extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let {handleClick, cartCountItems} = this.props

        return (
            <div>
                <div className="header-middle">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 clearfix w-50 header-middle-logo">
                                <div className="logo pull-left">
                                    <a href="/"><img src={require("../../images/home/logo_best2.png")}
                                                     alt=""/></a>
                                </div>
                                {/* TODO maybe released in the future*/}

                                {/*<div className="btn-group pull-right clearfix">*/}
                                {/*    <div className="btn-group">*/}
                                {/*        <button type="button" className="btn btn-default dropdown-toggle usa"*/}
                                {/*                data-toggle="dropdown">*/}
                                {/*            USA*/}
                                {/*            <span className="caret"></span>*/}
                                {/*        </button>*/}
                                {/*        <ul className="dropdown-menu">*/}
                                {/*            <li><a href="assets/pages/AppHome">Canada</a></li>*/}
                                {/*            <li><a href="assets/pages/AppHome">UK</a></li>*/}
                                {/*        </ul>*/}
                                {/*    </div>*/}

                                {/*    <div className="btn-group">*/}
                                {/*        <button type="button" className="btn btn-default dropdown-toggle usa"*/}
                                {/*                data-toggle="dropdown">*/}
                                {/*            DOLLAR*/}
                                {/*            <span className="caret"></span>*/}
                                {/*        </button>*/}
                                {/*        <ul className="dropdown-menu">*/}
                                {/*            <li><a href="assets/pages/AppHome">Canadian Dollar</a></li>*/}
                                {/*            <li><a href="assets/pages/AppHome">Pound</a></li>*/}
                                {/*        </ul>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                            </div>
                            <div className="col-md-8 clearfix w-50 header-middle-cart">
                                <div className="shop-menu clearfix pull-right">
                                    <ul className="nav navbar-nav">
                                        {/* TODO need release in the future*/}
                                        {/*<li><a href="assets/pages/AppHome"><i*/}
                                        {/*    className="fa fa-user"></i> Account</a></li>*/}
                                        {/*<li><a href="assets/pages/AppHome"><i*/}
                                        {/*    className="fa fa-star"></i> Wishlist</a></li>*/}
                                        {/*<li><a href="checkout.html"><i*/}
                                        {/*    className="fa fa-crosshairs"></i> Checkout</a></li>*/}
                                        <li>
                                            <button type={'button'} onClick={() => handleClick()}>
                                                <span className="fa-stack fa-2x has-badge" data-count={cartCountItems}>
                                                    <i className="fa fa-stack-2x fa-inverse"></i>
                                                    <i className="fa fa-shopping-cart fa-stack-2x"></i>
                                                </span>
                                            </button>
                                        </li>
                                        {/*<li><a href="login.html"><i className="fa fa-lock"></i> Login</a></li>*/}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

HeaderMiddle.propTypes = {};

export default HeaderMiddle;
