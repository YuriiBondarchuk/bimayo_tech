import React, {Component, Fragment} from 'react';
import {getSearchSuggestions} from "../api/SearchItemsSuggestions";

class Autocomplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSuggestion: -1,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ""
        }

        this.redirectToProduct = this.redirectToProduct.bind(this);
    }
    onChange = e => {
        const {countSuggestions} = this.props;
        const userInput = e.currentTarget.value;

        this.setState({
            userInput
        });

        if (userInput.length > 2) {
            getSearchSuggestions(userInput, countSuggestions).then(data => {
                this.setState({
                    activeSuggestion: -1,
                    filteredSuggestions: data.suggestions,
                    showSuggestions: true,
                });
            });
        }
    }

    onClick = (e, suggestion) => {
        this.setState({
            activeSuggestion: -1,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });

        this.redirectToProduct(e, suggestion);
    };

    onKeyDown = e => {

        const {activeSuggestion, filteredSuggestions} = this.state;

        if (e.keyCode === 13) {
            this.setState({
                activeSuggestion: -1,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion].name
            });
            this.redirectToProduct(e, filteredSuggestions[activeSuggestion]);
        } else if (e.keyCode === 38) {
            if (activeSuggestion === -1) {
                return;
            }
            this.setState({activeSuggestion: activeSuggestion - 1});
        }
        // User pressed the down arrow, increment the index
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }
            this.setState({activeSuggestion: activeSuggestion + 1});
        }
    };

    redirectToProduct(event, suggestion) {
        window.location = "/product/" + suggestion.url;
    }

    render() {
        const {
            onChange,
            onKeyDown,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <div className="dropdown-menu">
                        <ul>
                            {filteredSuggestions.map((suggestion, key) => {
                                    let className;

                                    // Flag the active suggestion with a class
                                    if (key === activeSuggestion) {
                                        className = "active";
                                    }
                                    return (
                                        <li className={"menu-item " + className} key={key}
                                            onClick={(e) => this.onClick(e, suggestion)}>
                                            <a className="product-element">
                                        <span className="image">
                                        <img className={"dropdown-suggestion-img"}
                                             src={JSON.parse(suggestion.offerPicture.pictures)[0]}/>
                                        </span>
                                                <span className="description">
                                            <span className="name">{suggestion.name}</span>
                                            <span>Bimayo<br/></span>
                                            <span className="os-price-available">{suggestion.price} ₴</span>
                                            <br/>
                                                    {suggestion.description.substr(0, 120).replace(/<\/?[^>]+(>|$)/g, "") + ' ...'}
                                        </span>
                                            </a>
                                        </li>
                                    );
                                }
                            )
                            }
                        </ul>
                    </div>
                );
            } else {
                suggestionsListComponent = (
                    <div className="no-suggestions">
                        <em>По Вашому запиту нічого не знайдено!</em>
                    </div>
                );
            }
        }

        return (
            <Fragment>
                <div className="col-6 parent_search_box">
                    <div className="search_box">
                        <input
                            type="text"
                            placeholder="Пошук"
                            onChange={onChange}
                            onKeyDown={onKeyDown}
                            value={userInput}
                        />
                        {suggestionsListComponent}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Autocomplete;