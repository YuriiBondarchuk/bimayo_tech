import React, {Component} from 'react';
import PropTypes from "prop-types";

class ProductInputSpinner extends Component {
    constructor(props) {
        super(props);

        const {count} = this.props

        this.state = {
            count: count
        }

        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.changeCartStorage = this.changeCartStorage.bind(this);
    }

    get count() {
        return this.state.count;
    }

    increment() {
        const {max, calculateTotalPrice, product, items} = this.props;

        if (typeof max === 'number' && this.count >= max) return;

        let newValue = this.count + 1;
        this.setState({count: newValue});

        if (calculateTotalPrice !== undefined) {
            let updatedProduct = calculateTotalPrice(items, product, newValue);

            this.changeCartStorage(updatedProduct);
        }
    }

    decrement() {
        const {min, calculateTotalPrice, product, items} = this.props;

        if (typeof min === 'number' && this.count <= min) return;

        let newValue = this.count - 1;
        this.setState({count: newValue});

        if (calculateTotalPrice !== undefined) {
            let updatedProduct = calculateTotalPrice(items, product, newValue);

            this.changeCartStorage(updatedProduct);
        }
    }

    changeCartStorage(product) {

        let items = JSON.parse(localStorage.getItem(global.config.const.STORAGE_CART));

        items[product.id] = product;

        this.setState({
            items: items
        })

        localStorage.setItem(global.config.const.STORAGE_CART, JSON.stringify(Object.assign({}, items)));
    }

    render() {
        const {count} = this.state
        return (
            <div className="input-number" style={this.props.style} key={count}>
                <button type="button" onClick={this.decrement}>&minus;</button>
                <span>{count}</span>
                <button type="button" onClick={this.increment}>&#43;</button>
            </div>
        )
    }
}

ProductInputSpinner.propTypes = {
    max: PropTypes.number,
    min: PropTypes.number,
    calculateTotalPrice: PropTypes.func,
    price: PropTypes.number,
    count: PropTypes.number.isRequired,
}

export default ProductInputSpinner;