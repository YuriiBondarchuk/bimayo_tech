import React, {Component} from 'react';
import {Button, Col, Form, Row} from "react-bootstrap";
import {sendMailComponent} from "../api/Mailer";
import PostSendModal from "./PostSendModal";

class QuestionForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            validated: false
        }

        this.formRef = React.createRef();
        this.userNameRef = React.createRef();
        this.userEmailRef = React.createRef();
        this.userQuestionRef = React.createRef();

        this.postCreateOrderModalRef = React.createRef();

        this.handleSubmit = this.handleSubmit.bind(this);
        this.clearQuestionForm = this.clearQuestionForm.bind(this)
    }

    clearQuestionForm() {
        this.userNameRef.current.value = '';
        this.userEmailRef.current.value = '';
        this.userQuestionRef.current.value = '';
    }

    handleSubmit(form) {

        dataLayer.push(
            {
                'event': 'submit',
                'page': 'Contact',
                'form':'Question_form',
                'action': 'button_submit'
            }
        );

        if (form.checkValidity() === false) {

            this.setState({
                validated: true
            });
        } else {
            let formData = {
                'userName': this.userNameRef.current.value,
                'userEmail': this.userEmailRef.current.value,
                'userQuestion': this.userQuestionRef.current.value
            }
            sendMailComponent(formData).then(data => {
                let modalData = [];
                if (data.successfully) {
                    modalData['headerMessage'] = 'Вітаємо!';
                    modalData['bodyMessage'] = data.message;
                    this.clearQuestionForm({}, true)
                } else {
                    modalData['headerMessage'] = 'Нажаль виникли проблеми!';
                    modalData['bodyMessage'] = data.message;
                }

                this.postCreateOrderModalRef.current.manageCartModal(modalData, data.successfully)
            })
        }
    };

    render() {
        let {validated} = this.state;
        return (
            <>
                <PostSendModal ref={this.postCreateOrderModalRef}/>
                <Form noValidate validated={validated} ref={this.formRef}>
                    <Row>
                        <Form.Group as={Col} md="4" controlId="validationCustom01" className="mb-3">
                            <Form.Control
                                required
                                type="text"
                                placeholder="Введіть ваше ім'я"
                                ref={this.userNameRef}
                            />
                            <Form.Control.Feedback type={'invalid'}>
                                Вкажіть Ваше ім'я
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationCustom02" className="mb-3">
                            <Form.Control
                                required
                                type="email"
                                placeholder="Ваша електронна адреса"
                                ref={this.userEmailRef}
                            />
                            <Form.Control.Feedback type={'invalid'}>
                                Поле електронної пошти є обов'язковим та повинно містити '@'
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Row>
                    <Row className="mb-3">
                        <Form.Group as={Col} md="8" controlId="validationCustom03">
                            <Form.Control as="textarea" placeholder="Сформулюйте Ваше запитання" required
                                          ref={this.userQuestionRef}
                            />
                            <Form.Control.Feedback type="invalid">
                                Будь-ласка напишіть Ваше запитання
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Row>
                    <Button type="button" onClick={() => this.handleSubmit(this.formRef.current)}>Надіслати</Button>
                </Form>
            </>
        );
    }
}

export default QuestionForm;