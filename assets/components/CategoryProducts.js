import React, {Component} from 'react';
import {getCategoryData} from "../api/Category";
import CategoryTree from "./CategoryTree";
import PropTypes from "prop-types";

class CategoryProducts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            category: [],
        }

        getCategoryData().then(data => {
            this.setState({
                category: data.category,
            })
        })

        this.renderCategoryTree = this.renderCategoryTree.bind(this)
    }

    renderCategoryTree(category, level, numberAccordian) {
        if (category !== undefined && Object.keys(category).length > 0) {
            return (<CategoryTree category={category} level={level} key={category.id} number={1} numberAccordian={numberAccordian}/>
            )
        }
    }

    render() {
        let categories = this.state.category
        let {numberAccordian} = this.props

        return (
            <div>
                <h2>Категорії</h2>
                {categories.map((arr) => (this.renderCategoryTree(arr, 0, numberAccordian)))}
            </div>
        )
            ;
    }
}

CategoryProducts.defaultProps = {
    numberAccordian: 1
}

CategoryProducts.propTypes = {
    numberAccordian: PropTypes.number.isRequired
}

export default CategoryProducts;