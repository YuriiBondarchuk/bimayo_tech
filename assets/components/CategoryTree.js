import React from 'react';
import PropTypes from "prop-types";

const CategoryTree = (props) => {

        const {category, level, numberAccordian} = props;

        if (level === 0) {
            return (
                <>
                    <div className="panel-heading main-category-tree-heading">
                        <h4 className="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian"
                               data-target={"#" + category.id + '_' + numberAccordian}>
                                <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                            </a>
                            <a href={'/products/' + category.url}><h2>{category.name}</h2></a>
                        </h4>
                    </div>
                    <div className="panel-group category-products panel-collapse collapse main-category-tree-products"
                         id={category.id + '_' + numberAccordian}>
                        <div className="panel panel-default">
                            {category.children !== undefined && Object.keys(category.children).map((key, i) =>
                                <CategoryTree category={category.children[key]} level={level + 1} key={i} numberAccordian={numberAccordian}/>
                            )}
                        </div>
                    </div>
                </>
            )
        } else if (category.children !== undefined) {
            return (
                <>
                    <div className="panel-heading">
                        <h4 className="panel-title">
                            <a href={'/products/' + category.url}><span>{category.name}</span></a>
                            <a data-toggle="collapse" data-parent="#accordian"
                               data-target={"#" + category.id + '_' + numberAccordian}>
                                <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                            </a>
                        </h4>
                    </div>
                    <div id={category.id + '_' + numberAccordian} className="panel-collapse collapse" key={category.id.toString()}>
                        <div className="panel-body">
                            <ul>
                                {Object.keys(category.children).map((key, i) =>
                                    <CategoryTree category={category.children[key]} level={level + 1} key={i} numberAccordian={numberAccordian}/>
                                )}
                            </ul>
                        </div>
                    </div>
                </>
            )
        } else if (category.level === 1) {
            return (
                <>
                    <div className="panel-heading">
                        <h4 className="panel-title">
                            <a href={"/products/" + category.url}>{category.name} ({category.count})</a>
                        </h4>
                    </div>
                </>
            )
        } else {
            return (
                <>
                    <li><a href={"/products/" + category.url}>{category.name} <span
                        className="badge pull-right">({category.count})</span></a></li>
                </>
            )
        }
    }
;

CategoryTree.propTypes = {
    category: PropTypes.object.isRequired,
    level: PropTypes.number.isRequired,
    numberAccordian: PropTypes.number.isRequired
}

export default CategoryTree;
