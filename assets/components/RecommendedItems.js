import React, {Component} from 'react';
import {getRecommendedItems} from "../api/RecommendedItems";

class RecommendedItems extends Component {
    constructor(props) {
        super(props);

        this.addToCart = 'Додати до кошика';

        this.state = {
            items: [],
        }

        const {category} = this.props;

        getRecommendedItems(category).then(data => {
            this.setState({
                items: this.breakArray(data.items),
            })
        })

        this.breakArray = this.breakArray.bind(this)
        this.redirectToProduct = this.redirectToProduct.bind(this)
    }

    redirectToProduct(event, vendorCode) {
        if (event.target.tagName === "IMG") {
            window.location = "/product/" + vendorCode;
        }
    }

    breakArray(array) {
        let size = 3; //размер подмассива
        let subarray = []; //массив в который будет выведен результат.
        for (let i = 0; i < Math.ceil(array.length / size); i++) {
            subarray[i] = array.slice((i * size), (i * size) + size);
        }
        return subarray;
    }

    render() {
        const {handleClick} = this.props;
        const {items} = this.state;
        return (<div className="recommended_items">
            <h2 className="title text-center">Рекомендовані товари</h2>
            <div id="recommended-item-carousel" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    {items.map((subItems, i) => (i === 0) ? <div className="item active" key={i}>
                        {subItems.map((item, i) => <div className="col-sm-4" key={i}>
                            <div className="product-image-wrapper">
                                <div className="single-products">
                                    <div className="productinfo text-center"
                                         style={{cursor: 'pointer'}}>
                                        <img src={JSON.parse(item.offerPicture.pictures)[0]} alt={item.name} title={item.name}
                                             onClick={(event) => this.redirectToProduct(event, item.url)}
                                        />
                                        <h2 className="d-inline product-price">{item.priceMargin} {item.currencyName}</h2>
                                        {
                                            (item.oldPrice !== undefined) ?
                                                <h2 className="price-before-discount d-inline">{item.oldPrice} {item.currencyName}</h2>
                                                : ''
                                        }
                                        <p>{item.name.split(' ').slice(0, 8).join(' ') + ' ...'}</p>
                                        <button className="btn btn-default add-to-cart"
                                                onClick={() => handleClick(item, 1)}>
                                            <i className="fa fa-shopping-cart"></i>{this.addToCart}
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>)}
                    </div> : <div className="item" key={i}>
                        {subItems.map((item, i) => <div className="col-sm-4" key={i}>
                            <div className="product-image-wrapper" style={{cursor: 'pointer'}}>
                                <div className="single-products">
                                    <div className="productinfo text-center">
                                        <img src={JSON.parse(item.offerPicture.pictures)[0]} alt={item.name} title={item.name}
                                             onClick={(event) => this.redirectToProduct(event, item.url)}
                                        />
                                        <h2>{item.priceMargin} {item.currencyName}</h2>
                                        <p>{item.name.split(' ').slice(0, 8).join(' ') + ' ...'}</p>
                                        <button className="btn btn-default add-to-cart"
                                                onClick={() => handleClick(item, 1)}>
                                            <i className="fa fa-shopping-cart"></i>{this.addToCart}
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>)}
                    </div>)}
                </div>
                <a className="left recommended-item-control"
                   href="assets/pages/AppHome#recommended-item-carousel"
                   data-slide="prev">
                    <i className="fa fa-angle-left"></i>
                </a>
                <a className="right recommended-item-control"
                   href="assets/pages/AppHome#recommended-item-carousel"
                   data-slide="next">
                    <i className="fa fa-angle-right"></i>
                </a>
            </div>
        </div>);
    }
}

export default RecommendedItems;