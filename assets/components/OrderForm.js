import React, {Component} from 'react';
import {Form} from "react-bootstrap";

class OrderForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            validatedShopperInfo: false,
            validatedAddressDelivery: false,
        }

        this.formShopperInfoRef = React.createRef();
        this.userNameRef = React.createRef();
        this.userSurnameRef = React.createRef();
        this.userPhoneRef = React.createRef();
        this.userEmailRef = React.createRef();

        this.formAddressDelivery = React.createRef();
        this.postCityRef = React.createRef();
        this.postOfficeRef = React.createRef();

        this.deliveryVariantOne = React.createRef();
        this.deliveryVariantTwo = React.createRef();

        this.paymentOptionOne = React.createRef();
        this.paymentOptionTwo = React.createRef();

        this.commentToOrder = React.createRef();

        this.handleSubmit = this.handleSubmit.bind(this);
        this.prepareOrderData = this.prepareOrderData.bind(this);
    }

    handleSubmit() {

        let orderData = {
            'isValidOrderData': true,
            'data': {}
        };

        let formShopperInfo = this.formShopperInfoRef.current;
        if (formShopperInfo.checkValidity() === false) {
            this.setState({
                validatedShopperInfo: true
            });

            orderData.isValidOrderData = false;
        }

        let formAddressDelivery = this.formAddressDelivery.current;
        if (formAddressDelivery.checkValidity() === false) {

            this.setState({
                validatedAddressDelivery: true
            });

            orderData.isValidOrderData = false;
        }

        if (orderData.isValidOrderData) {
            orderData.data = this.prepareOrderData()
        }

        return orderData;
    };

    prepareOrderData() {
        return {
            'name': this.userNameRef.current.value,
            'surName': this.userSurnameRef.current.value,
            'phone': this.userPhoneRef.current.value,
            'email': this.userEmailRef.current.value,
            'deliveryType': this.deliveryVariantOne.current.checked ? this.deliveryVariantOne.current.value : this.deliveryVariantTwo.current.value,
            'paymentType': this.paymentOptionOne.current.checked ? this.paymentOptionOne.current.value : this.paymentOptionTwo.current.value,
            'postCity': this.postCityRef.current.value,
            'postOffice': this.postOfficeRef.current.value,
            'comment': this.commentToOrder.current.value
        }
    }

    render() {
        let {validatedShopperInfo, validatedAddressDelivery} = this.state;
        return (
            <div className="shopper-informations">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="shopper-info">
                            <p>Контактні дані</p>
                            <Form noValidate validated={validatedShopperInfo} ref={this.formShopperInfoRef}>
                                <Form.Group controlId="validation-shopper-info1">
                                    <Form.Control
                                        required
                                        type="text"
                                        placeholder="Ваше ім'я"
                                        ref={this.userNameRef}
                                    />
                                    <Form.Control.Feedback type={'invalid'}>
                                        Вкажіть Ваше ім'я
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group controlId="validation-shopper-info2">
                                    <Form.Control
                                        required
                                        type="text"
                                        placeholder="Ваше прізвище"
                                        ref={this.userSurnameRef}
                                    />
                                    <Form.Control.Feedback type={'invalid'}>
                                        Вкажіть Ваше прізвище
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group controlId="validation-shopper-info3">
                                    <Form.Control
                                        required
                                        type="tel"
                                        pattern={'^(?:\\+38)?(0\\d{9})$'}
                                        placeholder="Ваш телефон"
                                        ref={this.userPhoneRef}
                                    />
                                    <Form.Control.Feedback type={'invalid'}>
                                        Вкажіть Ваше телефон
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group controlId="validation-shopper-info4">
                                    <Form.Control
                                        required
                                        type="email"
                                        name={'email'}
                                        placeholder="Електронна адреса"
                                        ref={this.userEmailRef}
                                    />
                                    <Form.Control.Feedback type={'invalid'}>
                                        Вкажіть Вашу електронну адреса
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Form>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="bill-to">
                            <p>Способи доставки</p>
                            <div className="form-one delivery-variant">
                                <Form noValidate>
                                    <Form.Group controlId="validation-delivery-variant1">
                                        <Form.Check
                                            required
                                            defaultChecked={true}
                                            type="radio"
                                            label={"Нова Пошта (доставка оплачується окремо, не включена у вартість"}
                                            name={"delivery-variant"}
                                            value={"1"}
                                            ref={this.deliveryVariantOne}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="validation-delivery-variant2">
                                        <Form.Check
                                            required
                                            type="radio"
                                            label={"Укрпошта. Доставка можлива за повної оплати за товар"}
                                            name={"delivery-variant"}
                                            value={"2"}
                                            ref={this.deliveryVariantTwo}
                                        />
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="bill-to">
                            <p>Способи оплати</p>
                            <div className="form-two payment-option">
                                <Form noValidate>
                                    <Form.Group controlId="validation-delivery-variant1">
                                        <Form.Check
                                            required
                                            defaultChecked={true}
                                            type="radio"
                                            label={"Оплата на банківську картку (Приват, Моно)"}
                                            name={"payment-option"}
                                            value={"1"}
                                            ref={this.paymentOptionOne}
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Check
                                            required
                                            type="radio"
                                            label={"Оплата після отримання посилки у відділенні."}
                                            name={"payment-option"}
                                            value={"2"}
                                            ref={this.paymentOptionTwo}
                                        />
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <div className="bill-to">
                            <p>Адреса доставки</p>
                            <div className="form-one">
                                <Form noValidate validated={validatedAddressDelivery} ref={this.formAddressDelivery}>
                                    <Form.Group controlId="validation-address-delivery1">
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Місто"
                                            ref={this.postCityRef}
                                        />
                                        <Form.Control.Feedback type={'invalid'}>
                                            Вкажіть Місто для доставки
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="validation-address-delivery2">
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Відділення пошти"
                                            ref={this.postOfficeRef}
                                        />
                                        <Form.Control.Feedback type={'invalid'}>
                                            Вкажіть № відділення пошти
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="order-message">
                            <p>Коментар до замовлення</p>
                            <textarea name="message"
                                      placeholder="Якщо Ви маєте запитання щодо замовлення залиште його тут"
                                      rows="3"
                                      ref={this.commentToOrder}
                            >
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default OrderForm;