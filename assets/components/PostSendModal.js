import React, {Component} from 'react';
import {Alert, Button, Modal} from "react-bootstrap";

class PostSendModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            alertVariant: 'success',
            modalData: []
        }

        this.handleClose = this.handleClose.bind(this)
        this.handleShow = this.handleShow.bind(this)
        this.manageCartModal = this.manageCartModal.bind(this)
        this.redirectToHome = this.redirectToHome.bind(this)
    }

    manageCartModal(modalData, successfully) {
        this.setState({
            modalData: modalData,
            alertVariant: successfully ? 'success' : 'danger'
        })

        this.handleShow();
    }

    handleClose() {
        this.setState({
            show: false
        })
    };

    handleShow() {
        this.setState({
            show: true,
        })
    };

    redirectToHome() {
        window.location = "/";
    }

    render() {
        let {show, alertVariant, modalData} = this.state;

        return (
            <>
                <Modal
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    show={show} onHide={this.handleClose}
                >
                    <Alert variant={alertVariant} className={"post-create-order"}>
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                                {modalData.headerMessage}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <p dangerouslySetInnerHTML={{__html: modalData.bodyMessage}}>
                            </p>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.redirectToHome}>Перейти на Головну</Button>
                        </Modal.Footer>
                    </Alert>
                </Modal>
            </>
        );
    }
}

export default PostSendModal;