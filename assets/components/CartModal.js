import React, {Component} from 'react';
import {Button, Modal} from "react-bootstrap";
import ProductInputSpinner from "./ProductInputSpinner";
import {addItemsToCartInStorage, removeItemsFromCartInStorage, calculateTotalPrice} from "../functions/functions";

class CartModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            items: {}
        }

        this.handleClose = this.handleClose.bind(this)
        this.handleShow = this.handleShow.bind(this)
        this.manageCartModal = this.manageCartModal.bind(this)
        this.addItemsToCart = this.addItemsToCart.bind(this)
        this.updateTotalPriceInState = this.updateTotalPriceInState.bind(this)
        this.removeItemsFromCart = this.removeItemsFromCart.bind(this)
        this.redirectToCheckout = this.redirectToCheckout.bind(this)
    }

    manageCartModal(product, count) {

        this.addItemsToCart(product, count);

        this.handleShow();
    }

    handleClose() {
        this.setState({
            show: false
        })
    };

    handleShow() {
        this.setState({
            show: true,
        })
    };

    addItemsToCart(product, count) {
        let items = addItemsToCartInStorage(product, count)

        this.setState({
            items: items
        })
    };

    removeItemsFromCart(product) {
        let items = removeItemsFromCartInStorage(product);

        this.setState({
            items: items
        })
        const {handleRemoveItemInCart} = this.props;

        handleRemoveItemInCart();
    };

    updateTotalPriceInState(items, product, count) {
        let updatedItems = calculateTotalPrice(items, product, count);

        this.setState({
            items: updatedItems
        })

        return updatedItems[product.id];
    };

    redirectToCheckout() {
        window.location = "/checkout";
    }

    render() {
        let {show, items} = this.state;

        return (
            <>
                <Modal show={show} onHide={this.handleClose} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Ваш кошик</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <section id="cart_items">
                            <div className="container w-auto">
                                <div className="table-responsive cart_info">
                                    <table className="table table-condensed">
                                        <thead>
                                        <tr className="cart_menu">
                                            <td className="image">Товар</td>
                                            <td className="description">Назва</td>
                                            <td className="price">Вартість</td>
                                            <td className="quantity">Кількість</td>
                                            <td className="total">Разом</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {Object.keys(items).length > 0 ?
                                            Object.keys(items).map((key, i) =>
                                                <tr key={key}>
                                                    <td className="cart_product col-sm-3">
                                                        <a
                                                            href={"/product/" + items[key]['url']}>
                                                            <img src={JSON.parse(items[key].offerPicture.pictures)[0]}
                                                                 alt=""/>
                                                        </a>
                                                    </td>
                                                    <td className="cart_description col-sm-3">
                                                        <h4><a
                                                            href={"/product/" + items[key]['url']}>{items[key]['name'].split(' ').slice(0, 2).join(' ') + ' ...'}</a>
                                                        </h4>
                                                        <p>код: {items[key]["vendorCode"]}</p>
                                                    </td>
                                                    <td className="cart_price col-sm-3">
                                                        <p>{items[key]['price']} {items[key]['currencyName']}</p>
                                                    </td>
                                                    <td className="cart_quantity col-sm-3">
                                                        <div className="cart_quantity_button">
                                                            <ProductInputSpinner
                                                                min={1} max={10}
                                                                count={items[key]['count']}
                                                                product={items[key]}
                                                                items={items}
                                                                calculateTotalPrice={this.updateTotalPriceInState}
                                                                key={i}
                                                            />
                                                        </div>
                                                    </td>
                                                    <td className="cart_total col-sm-3">
                                                        <p className="cart_total_price">{items[key]['totalPrice']} {items[key]['currencyName']}</p>
                                                    </td>
                                                    <td className="cart_delete col-sm-3">
                                                        <button className="cart_quantity_delete" type={"button"}
                                                                onClick={() => this.removeItemsFromCart(items[key])}
                                                        ><i
                                                            className="fa fa-times"></i></button>
                                                    </td>
                                                </tr>
                                            )
                                            :
                                            <tr>
                                                <td style={{border: 'none'}}><h4>Нажаль Ваш кошик порожній!</h4></td>
                                            </tr>
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.handleClose}>
                            Продовжити покупки
                        </Button>
                        <Button className={'btn-to-order'} onClick={this.redirectToCheckout}>
                            Оформити замовлення
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default CartModal;