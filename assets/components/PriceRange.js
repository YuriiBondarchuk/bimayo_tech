import React, {Component} from 'react';
import {getPriceRangeInput} from "../functions/functions";

class PriceRange extends Component {
    constructor(props) {
        super(props);

        this.state = {
            minPrice: 0,
            maxPrice: 0,
            invalidMinPrice: false,
            invalidMaxPrice: false,
            buttonDisabled: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeInputPrice = this.handleChangeInputPrice.bind(this);
        this.handleChangePriceMin = this.handleChangePriceMin.bind(this);
        this.handleChangePriceMax = this.handleChangePriceMax.bind(this);
        this.handleChangeByButton = this.handleChangeByButton.bind(this);
    }

    handleChange() {
        let priceRange = getPriceRangeInput().slider().data().slider.value;
        let minPrice = priceRange[0];
        let maxPrice = priceRange[1];

        const {applyPriceRangeFilters} = this.props
        applyPriceRangeFilters(minPrice, maxPrice)

        dataLayer.push(
            {
                'event': 'apply_filter',
                'action': 'by_range'
            }
        );
    }

    handleChangeByButton() {
        const {applyPriceRangeFilters} = this.props
        const {minPrice, maxPrice} = this.state

        applyPriceRangeFilters(minPrice, maxPrice)
        dataLayer.push(
            {
                'event': 'apply_filter',
                'action': 'by_button'
            }
        );
    }

    handleChangeInputPrice() {
        let priceRange = getPriceRangeInput().slider().data().slider.value

        if (typeof priceRange !== 'undefined') {
            let minPrice = priceRange[0];
            let maxPrice = priceRange[1];

            this.setState(
                {
                    minPrice: minPrice,
                    maxPrice: maxPrice,
                })
        }
    }
    handleChangePriceMin(e) {
        if (typeof e.target.value !== 'undefined') {
            const {min, max} = this.props
            if (Number(e.target.value) < min || Number(e.target.value) > max) {
                this.setState({
                    invalidMinPrice: true,
                    buttonDisabled: true
                })

            } else {
                this.setState({
                    invalidMinPrice: false,
                    buttonDisabled: false
                })
            }
            this.setState(
                {
                    minPrice: e.target.value,
                })
        }
    }

    handleChangePriceMax(e) {

        if (typeof e.target.value !== 'undefined') {
            const {min, max} = this.props
            if (Number(e.target.value) < Number(min) || Number(e.target.value) > Number(max)) {
                this.setState({
                    invalidMaxPrice: true,
                    buttonDisabled: true
                })
            } else {
                this.setState({
                    invalidMaxPrice: false,
                    buttonDisabled: false
                })
            }
            this.setState(
                {
                    maxPrice: e.target.value
                })
        }
    }

    componentDidMount() {
        getPriceRangeInput()
            .on('slideStop', () => this.handleChange())
            .on('slide', () => this.handleChangeInputPrice());
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.min !== this.props.min) {
            this.setState({
                minPrice: this.props.min
            })
        }
        if (prevProps.max !== this.props.max) {
            this.setState({
                maxPrice: this.props.max
            })
        }
    }

    render() {
        const {min, max, desktop} = this.props
        let {minPrice, maxPrice, invalidMinPrice, invalidMaxPrice, buttonDisabled} = this.state

        return (
            <div className="price-range" style={{visibility: "hidden"}} >{/*price - range*/}
                <h4>Ціна</h4>

                <div className="well text-center price-range-first-div">
                    <input min={min} max={max} type="number"
                           className={"form-control input-number " + (invalidMinPrice ? 'invalid' : '')}
                           value={minPrice}
                           onChange={(event) => this.handleChangePriceMin(event)}
                    />
                    <span> - </span>
                    <input min={min} max={max} type="number"
                           className={"form-control input-number " + (invalidMaxPrice ? 'invalid' : '')}
                           value={maxPrice}
                           onChange={(event) => this.handleChangePriceMax(event)}/>
                    <button type={"button"} className={"form-control " + (buttonDisabled ? 'button-inactive' : '')}
                            id={"priceRangeButton"}
                            disabled={buttonDisabled}
                            onClick={() => this.handleChangeByButton()}
                    >
                        ОК
                    </button>
                    <input type="range" className="span2 sl2"
                           data-slider-min={min}
                           data-slider-max={max}
                           data-slider-value={'[' + min + ',' + max + ']'}
                           id={desktop ? 'desktop' : 'mobile'}
                    /><br/>
                    <b className="pull-left">$ {min}</b> <b className="pull-right">$ {max}</b>
                </div>
            </div>
        );
    }
}

PriceRange.defaultProps = {
    desktop: false
}

export default PriceRange;