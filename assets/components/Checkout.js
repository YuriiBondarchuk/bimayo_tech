import React, {Component} from 'react';
import ProductInputSpinner from "./ProductInputSpinner";
import PostSendModal from "./PostSendModal";
import OrderForm from "./OrderForm";
import {calculateTotalPrice, addItemsToCartInStorage, removeItemsFromCartInStorage} from "../functions/functions";
import {createOrder} from "../api/Order";

class Checkout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: {},
            totalPrice: 0,
        }
        this.OrderRef = React.createRef();
        this.postCreateOrderModalRef = React.createRef();

        this.handleButtonClick = this.handleButtonClick.bind(this)
        this.addItemsToCart = this.addItemsToCart.bind(this)
        this.updateTotalPriceInState = this.updateTotalPriceInState.bind(this)
        this.removeItemsFromCart = this.removeItemsFromCart.bind(this)
        this.calculateAllPriceToPaid = this.calculateAllPriceToPaid.bind(this)
    }

    componentDidMount() {
        let items = JSON.parse(localStorage.getItem(global.config.const.STORAGE_CART));

        this.calculateAllPriceToPaid(items);

        this.setState({
            items: items,
        })
    }

    handleButtonClick() {
        let orderData = this.OrderRef.current.handleSubmit();

        if (orderData.isValidOrderData) {
            let {items, totalPrice} = this.state;
            orderData['items'] = items;
            orderData['data']['totalPrice'] = totalPrice;
            createOrder(orderData).then(data => {
                let modalData = [];
                if (data.successfully) {
                    modalData['headerMessage'] = 'Вітаємо!';
                    modalData['bodyMessage'] = data.message;
                    this.removeItemsFromCart({}, true)
                } else {
                    modalData['headerMessage'] = 'Нажаль виникли проблеми!';
                    modalData['bodyMessage'] = data.message;
                }

                this.postCreateOrderModalRef.current.manageCartModal(modalData, data.successfully)
            })
        }

    }

    addItemsToCart(product, count) {
        let items = addItemsToCartInStorage(product, count)

        this.setState({
            items: items
        })
    };

    removeItemsFromCart(product, clearCart) {
        let items = removeItemsFromCartInStorage(product, clearCart);

        this.setState({
            items: items
        })

        this.calculateAllPriceToPaid(items);

        const {handleRemoveItemInCart} = this.props;

        handleRemoveItemInCart();
    };

    updateTotalPriceInState(items, product, count) {
        let updatedItems = calculateTotalPrice(items, product, count);

        this.setState({
            items: updatedItems
        })

        this.calculateAllPriceToPaid(items);

        return updatedItems[product.id];
    };

    calculateAllPriceToPaid(items) {
        let totalPrice = 0;

        Object.keys(items).forEach(el => {
            totalPrice += items[el].totalPrice;
        })

        this.setState({
            totalPrice: totalPrice
        })
    };

    render() {
        let {items, totalPrice} = this.state

        return (
            <>
                <PostSendModal ref={this.postCreateOrderModalRef}/>
                <section id="cart_items">
                    <div className="container">
                        {/*<div className="breadcrumbs">*/}
                        {/*    <ol className="breadcrumb">*/}
                        {/*        <li><a href="#">Home</a></li>*/}
                        {/*        <li className="active">Check out</li>*/}
                        {/*    </ol>*/}
                        {/*</div>*/}
                        {/*<!--/breadcrums--> TODO need implemented in next release*/}

                        {/*<div className="step-one">*/}
                        {/*    <h2 className="heading">Step1</h2>*/}
                        {/*</div>*/}
                        {/*<div className="checkout-options">*/}
                        {/*    <h3>New User</h3>*/}
                        {/*    <p>Checkout options</p>*/}
                        {/*    <ul className="nav">*/}
                        {/*        <li>*/}
                        {/*            <label><input type="checkbox"/> Register Account</label>*/}
                        {/*        </li>*/}
                        {/*        <li>*/}
                        {/*            <label><input type="checkbox"/> Guest Checkout</label>*/}
                        {/*        </li>*/}
                        {/*        <li>*/}
                        {/*            <a href=""><i className="fa fa-times"></i>Cancel</a>*/}
                        {/*        </li>*/}
                        {/*    </ul>*/}
                        {/*</div>*/}
                        {/*<!--/checkout-options--> TODO need implement after implement register functionality*/}

                        {/*<div className="register-req">*/}
                        {/*    <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as*/}
                        {/*        Guest</p>*/}
                        {/*</div>*/}
                        {/*<!--/register-req-->*/}
                        <OrderForm ref={this.OrderRef}/>
                        <div className="review-payment">
                            <h2>Огляд та Замовлення</h2>
                        </div>
                        <div className="table-responsive cart_info">
                            <table className="table table-condensed">
                                <thead>
                                <tr className="cart_menu">
                                    <td className="image">Товар</td>
                                    <td className="description">Назва</td>
                                    <td className="price">Вартість</td>
                                    <td className="quantity">Кількість</td>
                                    <td className="total">Разом</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                {Object.keys(items).length > 0 ?
                                    Object.keys(items).map((key, i) =>
                                        <tr key={key}>
                                            <td className="cart_product col-sm-3">
                                                <a href={"/product/" + items[key]['url']}>
                                                    <img src={JSON.parse(items[key].offerPicture.pictures)[0]} alt=""/>
                                                </a>

                                            </td>
                                            <td className="cart_description col-sm-3">
                                                <h4>
                                                    <a href={"/product/" + items[key]['url']}>
                                                        {items[key]['name'].split(' ').slice(0, 2).join(' ') + ' ...'}
                                                    </a>
                                                </h4>
                                                <p>код: {items[key]["vendorCode"]}</p>
                                            </td>
                                            <td className="cart_price col-sm-3">
                                                <p>{items[key]['price']} {items[key]['currencyName']}</p>
                                            </td>
                                            <td className="cart_quantity col-sm-3">
                                                <div className="cart_quantity_button">
                                                    <ProductInputSpinner
                                                        min={1} max={10}
                                                        count={items[key]['count']}
                                                        product={items[key]}
                                                        items={items}
                                                        calculateTotalPrice={this.updateTotalPriceInState}
                                                        key={i}
                                                    />
                                                </div>
                                            </td>
                                            <td className="cart_total col-sm-3">
                                                <p className="cart_total_price">{items[key]['totalPrice']} {items[key]['currencyName']}</p>
                                            </td>
                                            <td className="cart_delete col-sm-3">
                                                <button className="cart_quantity_delete" type={"button"}
                                                        onClick={() => this.removeItemsFromCart(items[key], false)}
                                                ><i
                                                    className="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                    )
                                    :
                                    <tr>
                                        <td style={{border: 'none'}}><h4>Нажаль Ваш кошик порожній!</h4></td>
                                    </tr>
                                }
                                <tr>
                                    <td colSpan="4" className="total-result">
                                        Загальна вартість
                                    </td>
                                    <td colSpan="2" className="total-result">
                                        <span>{totalPrice + ' UAH'} </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="order-create">
                            <button type={"button"} className={"btn btn-primary btn-lg"}
                                    onClick={() => this.handleButtonClick()}
                            >
                                Створити замовлення
                            </button>
                        </div>
                    </div>
                </section>
                {/* <!--/#cart_items-->*/}
            </>
        )
            ;
    }
}

export default Checkout;


