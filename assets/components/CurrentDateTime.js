import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CurrentDateTime extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentTime: this.getCurrentTime(),
            currentData: null
        }

        this.getCurrentTime = this.getCurrentTime.bind(this);
        this.getCurrentData = this.getCurrentData.bind(this);
        this.prepareTime = this.prepareTime.bind(this);
    }

    getCurrentTime() {
        let today = new Date();
        return this.prepareTime(today.getHours()) + ':' + this.prepareTime(today.getMinutes()) + ':' + this.prepareTime(today.getSeconds())
    }

    getCurrentData() {
        let today = new Date();
        let month = today.toLocaleString('default', {month: 'short'});

        return today.getDate() + ' ' + month.toUpperCase() + ' ' + today.getFullYear()
    }

    prepareTime(number) {
        return number.toString().length < 2 ? ('0' + number) : number;
    }

    componentDidMount() {
        this.interval = setInterval(() => this.setState({currentTime: this.getCurrentTime()}), 1000);
        this.setState({
            currentData: this.getCurrentData()
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <ul id={'date-time'}>
                {/*<li><a href=""><i className="fa fa-user"></i>EUGEN</a></li>*/} {/*TODO need added after do login and registration page*/}
                <li><a href=""><i className="fa fa-clock-o"></i>{this.state.currentTime}</a></li>
                <li><a href=""><i className="fa fa-calendar-o"></i>{this.state.currentData}</a></li>
            </ul>
        );
    }
}

CurrentDateTime.propTypes = {};

export default CurrentDateTime;
