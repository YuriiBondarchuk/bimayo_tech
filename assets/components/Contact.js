import React, {Component} from 'react';
import {getHeaderData} from "../api/Header";
import QuestionForm from "./QuestionForm";
import Breadcrumbs from "./Breadcrumbs";

const breadcrumbItems = window.breadcrumbs;

class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: 0
        }

        getHeaderData().then(data => {
            this.setState({
                phone: data.info.phone,
                email: data.info.email
            })
        })

        dataLayer.push(
            {
                'event': 'page_load',
                'page': 'Contact'
            }
        );
    }

    render() {
        let {phone, email} = this.state;

        return (
            <div id="contact-page" className="container">
                <Breadcrumbs items={breadcrumbItems}/>
                <div className="bg">
                    <div className="row">
                        <div className="col-sm-12">
                            <h2 className="title text-center">Контакти</h2>
                            <h4 className="title text-center">
                                У вас виникли запитання, зауваження, пропозиції?<br/>
                                Зв'яжіться з нами або заповніть, будь ласка, форму зворотного зв'язку.
                            </h4>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-8">
                            <div className="" id="question">
                                <h2 className="title text-center">Напишіть нам</h2>
                                <QuestionForm/>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="contact-info">
                                <h2 className="title text-center">Контактна інформація</h2>
                                <address>
                                    <p>Інтернет-магазин <b>«BIMAYO»</b></p>
                                    <p>тел: {phone}</p>
                                    <p>Email: {email}</p>
                                </address>
                                {/*TODO will be implemented in the future*/}
                                {/*<div className="social-networks">*/}
                                {/*    <h2 className="title text-center">Ми в соц мережах</h2>*/}
                                {/*    <ul>*/}
                                {/*        <li>*/}
                                {/*            <a href="#"><i className="fa fa-facebook"></i></a>*/}
                                {/*        </li>*/}
                                {/*        <li>*/}
                                {/*            <a href="#"><i className="fa fa-twitter"></i></a>*/}
                                {/*        </li>*/}
                                {/*        <li>*/}
                                {/*            <a href="#"><i className="fa fa-google-plus"></i></a>*/}
                                {/*        </li>*/}
                                {/*        <li>*/}
                                {/*            <a href="#"><i className="fa fa-youtube"></i></a>*/}
                                {/*        </li>*/}
                                {/*    </ul>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;