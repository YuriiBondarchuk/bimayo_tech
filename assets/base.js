/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (base.css in this case)
import './styles/base.css';

// start the Stimulus application
import './bootstrap';

//import js file
import './js/jquery';
import './js/bootstrap.min.js';
import './js/jquery.scrollUp.min.js';
import './js/jquery.prettyPhoto.js';
import './js/price-range.js';
import './js/main.js';
import './js/config.js';
// import './js/bootstrap-collapse.js';
import  './js/mobile_left_menu.js'
import  './js/mobile_left_filters.js'
import './js/modernizr-events.js'
