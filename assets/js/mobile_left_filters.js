$(document).ready(function () {
    $(function () {
        let $filters_popup = $('.filters-popup');

        $(".filters-trigger").click(function () {
            $('body').addClass('body_pointer');
            $filters_popup.show(0);
            $filters_popup.animate({left: parseInt($filters_popup.css('left'), 10) === 0 ? -$filters_popup.outerWidth() : 0}, 300);
            return false;
        });

        $(".filters-close").click(function () {
            $('body').removeClass('body_pointer');
            $filters_popup.animate({left: parseInt($filters_popup.css('left'), 10) === 0 ? -$filters_popup.outerWidth() : 0}, 300, function () {
                $filters_popup.hide(0);
            });
            return false;
        });

        // console.log(window.matchMedia('(max-width: 768px)')) //TODO maybe use in the future
        if ($(window).width() < 480 || $(window).height() < 480) {
            $(document).on('click', function (e) {
                if (!$(e.target).closest('.filters-popup').length) {
                    $('body').removeClass('body_pointer');
                    $filters_popup.animate({left: parseInt($filters_popup.css('left'), 10) === 0 ? -$filters_popup.outerWidth() : 0}, 300, function () {
                        $filters_popup.hide(0);
                    });
                }
            });
        }
    });
});
