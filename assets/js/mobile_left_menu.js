$(document).ready(function () {
    $(function () {
        let $menu_popup = $('.menu-popup');

        $(".menu-trigger").click(function () {
            $('body').addClass('body_pointer');
            $menu_popup.show(0);
            $menu_popup.animate({left: parseInt($menu_popup.css('left'), 10) === 0 ? -$menu_popup.outerWidth() : 0}, 300);
            return false;
        });

        $(".menu-close").click(function () {
            $('body').removeClass('body_pointer');
            $menu_popup.animate({left: parseInt($menu_popup.css('left'), 10) === 0 ? -$menu_popup.outerWidth() : 0}, 300, function () {
                $menu_popup.hide(0);
            });
            return false;
        });

        // console.log(window.matchMedia('(max-width: 768px)')) //TODO maybe use in the future
        if ($(window).width() < 480 || $(window).height() < 480) {
            $(document).on('click', function (e) {
                if (!$(e.target).closest('.menu-popup').length) {
                    $('body').removeClass('body_pointer');
                    $menu_popup.animate({left: parseInt($menu_popup.css('left'), 10) === 0 ? -$menu_popup.outerWidth() : 0}, 300, function () {
                        $menu_popup.hide(0);
                    });
                }
            });
        }
    });
});
