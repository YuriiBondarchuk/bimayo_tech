import {fetchJson} from './fecth_base.js';

export function createOrder(orderData) {
    return fetchJson(`/api/checkout/order`, {
        method: 'POST',
        body: JSON.stringify(
            {
                orderData: orderData,
            }
        )
    })
}