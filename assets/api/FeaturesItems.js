import {fetchJson} from './fecth_base.js';

export function getFeatureItems(random, pageNumber = 0, categoryUrl, filters) {
    return fetchJson(`/api/featureItems`, {
        method: 'POST',
        dataType:'json',
        cache: 'no-cache',
        body: JSON.stringify(
            {
                randomOffers: random,
                pageNumber: pageNumber,
                categoryUrl: categoryUrl,
                filters: filters
            }
        )
    })
}