export async function fetchJson(url, options) {
    let headers = {
        'Content-Type': 'application/json'
    };

    if (options && options.headers) {
        headers = {...options.headers, headers};
        delete options.headers;
    }

    return await fetch(url, Object.assign({
        credentials: 'same-origin',
        headers: headers,
    }, options))
        .then(checkStatus)
        .then(response => {
            return response.text()
                // .then(text => text ? JSON.parse(text) : '')
                .then(text => text ? parseJsonString(text) : '')
        });
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 400) {
        return response;
    }

    const error = new Error(response.statusText);
    error.response = response;

    throw error;
}

function parseJsonString(str) {
    let json = isJsonString(str) ? JSON.parse(str) : JSON.parse(str.slice(str.indexOf('}') + 1));
    str = null;

    return json;
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}



