import {fetchJson} from './fecth_base.js';

export function getRecommendedItems(category = false) {
    return fetchJson(`/api/recommendedItems`, {
            method: 'POST',
            dataType: 'json',
            cache: 'no-cache',
            body: JSON.stringify(
                {
                    category
                }
            )
        }
    )
}