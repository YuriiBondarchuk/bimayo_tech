import {fetchJson} from './fecth_base.js';

export function getSearchSuggestions(likeSql, count) {
    return fetchJson(`/api/searchSuggestions`, {
        method: 'POST',
        dataType: 'json',
        cache: 'no-cache',
        body: JSON.stringify(
            {
                countSuggestions: count,
                likeSql
            }
        )
    })
}