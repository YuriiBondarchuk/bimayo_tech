import ReactDOM from "react-dom";
import React, {useState} from "react";
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import CartModal from "../components/CartModal";
import Contact from "../components/Contact";
import RecommendedItems from "../components/RecommendedItems";
import {getCountItemsInCartFromStorage} from "../functions/functions";

function ContactPage() {

    const [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage())

    const CartRef = React.createRef();

    const handleClick = (product, count) => {
        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage());
    };

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    dataLayer.push(
        {
            'event': 'page_load',
            'page': 'Checkout'
        }
    );

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems}/>
            <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
            <Contact/>
            <RecommendedItems handleClick={handleClick}/>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<ContactPage/>, document.getElementById('AppContact'));