import {render} from "react-dom";
import React from "react";
import errorImage from "../images/errors/404.png";
import logo from "../images/home/logo_best.png";

render(
    <div>
        <div className="container text-center">
            <div className="logo-404">
                <a href="/"><img src={logo} alt=""/></a>
            </div>
            <div className="content-404">
                <img src={errorImage} className="img-responsive" alt=""/>
                <h1><b>От Халепа!</b> Ми не змогли знайти цю сторінку</h1>
                <p>Так так так... Схоже, ви щось зламали :) Сторінка, яку ви шукаєте, зникла :)</p>
                <h2><a href="/">Верніть мене на Сайт</a></h2>
            </div>
        </div>
    </div>, document.getElementById('AppError')
);