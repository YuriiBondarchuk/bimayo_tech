import ReactDOM from "react-dom";
import React, {useState} from "react";
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import Checkout from "../components/Checkout";
import CartModal from "../components/CartModal";
import {getCountItemsInCartFromStorage} from "../functions/functions";

function CheckoutPage() {

    const CartRef = React.createRef();

    const [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage())

    const handleClick = (product, count) => {
        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage());
    };

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    dataLayer.push(
        {
            'event': 'page_load',
            'page': 'Checkout'
        }
    );

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems}/>
            <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
            <Checkout handleRemoveItemInCart={handleRemoveItemInCart}/>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<CheckoutPage/>, document.getElementById('AppCheckout'));
