import React, {Component, useState} from "react";
import ReactDOM from 'react-dom';
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import RecommendedItems from "../components/RecommendedItems";
import FeaturesItems from "../components/FeaturesItems";
import CategoryProducts from "../components/CategoryProducts";
import CartModal from "../components/CartModal";
import Breadcrumbs from "../components/Breadcrumbs";
import {getCountItemsInCartFromStorage, isMobile} from "../functions/functions";

function Home() {
    const breadcrumbItems = window.breadcrumbs;

    const [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage());

    const CartRef = React.createRef();

    const handleClick = (product, count) => {
        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage())

        dataLayer.push(
            {
                'event': 'add_to_cart',
                'page': 'Home'
            }
        );
    }

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems}/>
            {/* TODO need released in the future*/}
            {/*<SliderTop/>*/}
            <section>
                <div className="container">
                    <Breadcrumbs items={breadcrumbItems}/>
                    <div className="row">
                        <div className="col-sm-3 parent-left-sidebar">
                            <div className="left-sidebar">
                                {
                                    !isMobile()
                                        ? <CategoryProducts/>
                                        : <></>
                                }
                                {/*TODO maybe will implement in the future*/}
                                {/*<BrandsProducts/>*/}
                                {/*TODO REQUIRED will implement in the future*/}
                                {/*<PriceRange/>*/}
                                {/*TODO maybe will implement in the future*/}
                                {/*shipping*/}
                                {/*<div className="shipping text-center">*/}
                                {/*    <img src={require("../images/home/shipping.jpg")} alt=""/>*/}
                                {/*</div>*/}
                                {/*shipping*/}
                            </div>
                        </div>
                        <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
                        <div className="col-sm-9 padding-right">
                            <FeaturesItems
                                title={"Гарячі пропозиції"}
                                pagination={false}
                                randomOffers={true}
                                categoryUrl={''}
                                handleClick={handleClick}
                            />
                            {/*features_items*/}
                            {/*TODO will implement in the future*/}
                            {/*<HomeCategoryTab/>*/}
                            <RecommendedItems handleClick={handleClick}/>
                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<Home/>, document.getElementById('AppHome'));