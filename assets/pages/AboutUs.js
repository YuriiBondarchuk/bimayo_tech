import ReactDOM from "react-dom";
import React, {useState} from "react";
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import CartModal from "../components/CartModal";
import CategoryProducts from "../components/CategoryProducts";
import RecommendedItems from "../components/RecommendedItems";
import Breadcrumbs from "../components/Breadcrumbs";
import {getCountItemsInCartFromStorage, isMobile} from "../functions/functions";

function AboutUs() {

    const [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage())

    const breadcrumbItems = window.breadcrumbs;

    const CartRef = React.createRef();

    const handleClick = (product, count) => {
        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage());
    };

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems}/>
            <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
            <section>
                <div className="container">
                    <Breadcrumbs items={breadcrumbItems}/>
                    <div className="row">
                        <div className="col-sm-3 parent-left-sidebar">
                            <div className="left-sidebar">
                                {
                                    !isMobile()
                                        ? <CategoryProducts/>
                                        : <></>
                                }
                            </div>
                        </div>
                        <div className="col-sm-9 padding-right">
                            <h2 className="title text-center">Про Нас</h2>
                            <div className="about-us"><p>
                                <strong>Нам дуже приємно познайомитися з Вами та розказати про нас!</strong></p>
                                <p>Наш інтернет-магазин «BIMAYO» створений для того, щоб кожен міг обрати свою річ з
                                    нашою
                                    кваліфікованою допомогою!</p>
                                <p>Інтернет-магазин «BIMAYO» є вірним помічником для наших клієнтів у створенні
                                    ідеальних образів на кожен день!</p>

                                <p><strong>Як це Вам допоможе!?</strong></p>

                                <p>Відразу після оформлення заявки на сайті, зможемо зв’язатися із кожним та уточнити
                                    побажання щодо товару (підтвердити розмір, доставку, оплату) та відправити бажану
                                    річ.</p>
                                <p><strong>То Ви хочете знати чим ми відрізняємося від інших:</strong></p>
                                <ul>
                                    <li>широким асортиментом речей, які поєднують якість, вишуканість та
                                        доступність, та спеціально відібрані завдяки доступним цінам
                                    </li>
                                    <li>професійними послугами правильного підбору розміру, фасону та форми білизни</li>
                                    <li>індивідуальним підходом до кожного клієнта
                                    </li>
                                    <li>великою пропозицією товарів домашнього одягу, щоб кожен міг почувати себе
                                        комфортно
                                        та міг підкреслити свою красу та особистість.
                                    </li>
                                </ul>

                                <p><strong>Також додаткові зручності для Вас:</strong></p>

                                <ul>
                                    <li>Швидка відповідь на отримане замовлення</li>
                                    <li>Доброзичливі поради з турботою та повагою</li>
                                    <li>Миттєва відправка зручним способом після підтвердження</li>
                                </ul>

                                <p><strong>Відправляємо щодня!</strong></p>

                                <p>Замовляєте через сайт, менеджер зв’язується, підтверджуєте замовлення, оплачуєте чи
                                    бажаєте оплату при отриманні, ми відразу відправляємо!</p>

                                <p>Вподобана річ уже їде до ВАС!</p>
                                <br/>
                                <p><strong>СПОСОБИ ОПЛАТИ:</strong></p>

                                <ol>
                                    <li>Безготівковий платіж на картку Visa та MasterCard</li>
                                </ol>

                                {/*<ul>*/}
                                {/*    /!*<li>Онлайн оплата карткою visa / master card</li>*!/*/}
                                {/*    <li>У касі банку або терміналі самообслуговування</li>*/}
                                {/*</ul>*/}

                                <ol>
                                    <li value="2">Оплата готівкою у відділенні Нової Пошти (післяплата).</li>
                                </ol>
                                <br/>
                                <p><strong>СПОСОБИ ДОСТАВКИ:</strong></p>

                                <ol>
                                    <li>Доставка по Україні</li>
                                </ol>

                                <ul>
                                    <li>Здійснюється протягом 1-4 днів кур'єрською службою Нова пошта чи Укрпошта</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <RecommendedItems handleClick={handleClick}/>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<AboutUs/>, document.getElementById('AppAboutUs'));