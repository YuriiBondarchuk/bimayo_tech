import ReactDOM from "react-dom";
import React, {useState} from "react";
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import CategoryProducts from "../components/CategoryProducts";
import RecommendedItems from "../components/RecommendedItems";
import ProductCategoryTab from "../components/ProductCategoryTab";
import SliderFade from "../components/SliderFade";
import {Form, Col, Row, Container} from "react-bootstrap";
import ProductInputSpinner from "../components/ProductInputSpinner";
import CartModal from "../components/CartModal";
import Breadcrumbs from "../components/Breadcrumbs";
import {getCountItemsInCartFromStorage, isMobile} from "../functions/functions";


function Product() {

    const [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage());

    let productData = window.productData;
    let addToCart = 'До кошика';

    const CartRef = React.createRef();
    const InputSpinnerRef = React.createRef();
    const productColorRef = React.createRef();
    const productSizeRef = React.createRef();

    const breadcrumbItems = window.breadcrumbs;

    const handleClick = (product, count) => {
        if (product !== undefined) {
            product['size'] = productSizeRef.current.value;
            product['color'] = productColorRef.current.value;
        }

        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage());

        dataLayer.push(
            {
                'event': 'add_to_cart',
                'page': 'Product'
            }
        );
    };

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems}/>
            <section>
                <div className="container">
                    <Breadcrumbs items={breadcrumbItems}/>
                    <div className="row">
                        <div className="col-sm-3 parent-left-sidebar">
                            <div className="left-sidebar">
                                {
                                    !isMobile()
                                        ? <CategoryProducts/>
                                        : <></>
                                }
                            </div>
                        </div>
                        <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
                        <div className="col-sm-9 padding-right">
                            <div className="product-details">{/*product-details*/}
                                <div className="col-sm-5">
                                    <SliderFade pictures={productData.pictures} imageAlt={productData.name}/>
                                </div>
                                <div className="col-sm-7">
                                    {/*product-information*/}
                                    <div className="product-information">
                                        <h2>{productData.name}</h2>
                                        <p>Код: {productData.vendorCode}</p>
                                        <span>
                        			<span style={{float: "none"}}>{productData.priceMargin} {productData.currencyName} </span>
                                        <span style={{float: "none"}}
                                              className="price-before-discount">{productData.oldPrice} {productData.currencyName} </span>
                                        <Container className={'container-product-information'}>
                                            <p><b>В наявності:</b>{productData.available ? " Так" : " Ні"}</p>
                                        <Row className="mb-2"
                                             style={{display: productData.existColor.length === 0 ? 'none' : 'block'}}
                                        >
                                            <Col sm={3}>
                                                <Form.Label>Колір: </Form.Label>
                                                <Form.Select size="lg" ref={productColorRef}>
                                                    {productData.existColor.map((key, i) => (
                                                        <option key={i}>{key}</option>
                                                    ))}
                                                </Form.Select>
                                            </Col>
                                        </Row>
                                        <Row className="mb-2"
                                             style={{display: productData.existSize.length === 0 ? 'none' : 'block'}}
                                        >
                                            <Col sm={3}>
                                                <Form.Label>Розмір:</Form.Label>
                                                <Form.Select size="lg" ref={productSizeRef}>
                                                    {productData.existSize.map((key, i) => (
                                                        <option key={i}>{key}</option>
                                                    ))}
                                                </Form.Select>
                                            </Col>
                                        </Row>
                                            <Row className="mb-2">
                                            <Col sm={3}>
                                                <Form.Label>Кількість:</Form.Label>
                                                <ProductInputSpinner min={1} max={10} count={1} ref={InputSpinnerRef}/>
                                            </Col>
                                        </Row>
                                        <Row className="mb-2">
                                            <Col sm={3}>
                                                <button type="button" className="btn cart"
                                                        onClick={() => handleClick(productData, InputSpinnerRef.current.count)}>
                                                    <i className="fa fa-shopping-cart"></i>
                                                    {addToCart}
                                                </button>
                                            </Col>
                                        </Row>
                                    </Container>
                        		</span>
                                    </div>
                                    {/*product-information*/}
                                </div>
                            </div>
                            {/*product-details*/}
                            <ProductCategoryTab params={productData.params} descriptions={productData.description}/>

                            <RecommendedItems handleClick={handleClick} category={productData.categoryImportId}/>
                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<Product/>, document.getElementById('AppProduct'));