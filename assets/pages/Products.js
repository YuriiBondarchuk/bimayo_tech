import ReactDOM from "react-dom";
import React, {useState} from "react";
import HeaderMain from "../components/Header/HeaderMain.js";
import Footer from "../components/Footer";
import FeaturesItems from "../components/FeaturesItems";
import CategoryProducts from "../components/CategoryProducts";
import CartModal from "../components/CartModal";
import Breadcrumbs from "../components/Breadcrumbs";
import {getCountItemsInCartFromStorage, isMobile} from "../functions/functions";
import PriceRange from "../components/PriceRange";

function Products() {

    // const pageName =
    const breadcrumbItems = window.breadcrumbs;
    const categoryTitle = window.breadcrumbs[window.breadcrumbs.length - 1].title;

    let [cartCountItems, setCartCountItems] = useState(getCountItemsInCartFromStorage());
    let [priceRange, setPriceRange] = useState({min: 0, max: 0});

    const getCategoryUrlFromRoute = function () {

        const pathName = window.location.pathname;

        return pathName.split('/').pop();
    };

    getCategoryUrlFromRoute()

    const CartRef = React.createRef();

    const FeaturesItemsRef = React.createRef();

    const handleClick = (product, count) => {
        CartRef.current.manageCartModal(product, count);

        setCartCountItems(getCountItemsInCartFromStorage());

        dataLayer.push(
            {
                'event': 'add_to_cart',
                'page': 'Products'
            }
        );
    };

    const handleRemoveItemInCart = () => {
        setCartCountItems(getCountItemsInCartFromStorage());
    };

    const setMinMaxPriceRange = (priceRange) => {
        setPriceRange({min: priceRange.min, max: priceRange.max});

        $('.sl2').slider();

        let priceRangeObjects = $('.price-range');

        let countPriceRange = priceRangeObjects.length;

        for (let i = 0; i < countPriceRange; i++) {
            priceRangeObjects[i].style.visibility = 'visible';
        }
    };

    const applyPriceRangeFilters = (min, max) => {
        let filters = {'priceRange': {min, max}};

        FeaturesItemsRef.current.applyFilters(filters)
    };

    return (
        <div>
            <HeaderMain handleClick={handleClick} cartCountItems={cartCountItems} minPriceRange={priceRange.min} maxPriceRange={priceRange.max}
                        applyPriceRangeFilters={applyPriceRangeFilters} showFilters={true}/>
            <section>
                <div className="container">
                    <Breadcrumbs items={breadcrumbItems}/>
                    <div className="row">
                        <div className="col-sm-3 parent-left-sidebar">
                            <div className="left-sidebar">
                                <PriceRange min={priceRange.min} max={priceRange.max}
                                            applyPriceRangeFilters={applyPriceRangeFilters} desktop={true}/>
                                {
                                    !isMobile()
                                        ? <CategoryProducts/>
                                        : <></>
                                }
                            </div>
                        </div>
                        <CartModal ref={CartRef} handleRemoveItemInCart={handleRemoveItemInCart}/>
                        <div className="col-sm-9 padding-right">
                            <FeaturesItems
                                title={categoryTitle}
                                pagination={true}
                                randomOffers={false}
                                categoryUrl={getCategoryUrlFromRoute()}
                                handleClick={handleClick}
                                applyPriceRangeFilters={applyPriceRangeFilters}
                                setMinMaxPriceRange={setMinMaxPriceRange}
                                ref={FeaturesItemsRef}
                            />
                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </div>
    );
}

ReactDOM.render(<Products/>, document.getElementById('AppProducts'));