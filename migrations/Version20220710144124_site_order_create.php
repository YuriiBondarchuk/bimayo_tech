<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220710144124_site_order_create extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'order';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'CREATE TABLE %s(
                    id SERIAL NOT NULL PRIMARY KEY,
                    user_name VARCHAR(255) NOT NULL,
                    user_surname VARCHAR(255) NOT NULL,
                    user_email VARCHAR(255) NOT NULL,
                    user_phone VARCHAR(255) NOT NULL,
                    status INTEGER NOT NULL CHECK(status IN (1,2,3,4)),
                    payment_type INTEGER NOT NULL CHECK(payment_type IN (1,2)),
                    delivery_type INTEGER NOT NULL CHECK(delivery_type IN (1,2)),
                    post_city TEXT NOT NULL,
                    post_office TEXT NOT NULL,
                    order_total_price FLOAT NOT NULL DEFAULT 0,
                    comment TEXT,
                    t_ins TIMESTAMPTZ(0) DEFAULT CURRENT_TIMESTAMP(0) NOT NULL,
                    t_upd TIMESTAMPTZ(0)
                )',
                self::FULL_NAME
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP TABLE %s', self::FULL_NAME));
    }
}
