<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221101105745_site_category_alter_and_add_column_url extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'category';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s ADD COLUMN url varchar(255)', self::FULL_NAME));
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s DROP COLUMN url;', self::FULL_NAME));
    }
}
