<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111084508_site_offer_add_column_old_price extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s ADD COLUMN old_price FLOAT', self::FULL_NAME));
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s DROP COLUMN old_price;', self::FULL_NAME));
    }
}
