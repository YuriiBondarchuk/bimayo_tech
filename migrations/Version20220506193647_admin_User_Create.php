<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220506193647_admin_User_Create extends AbstractMigration
{
    private const SCHEMA_NAME = 'admin';
    private const TABLE_NAME = '"user"';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'CREATE TABLE %s(
                    id SERIAL NOT NULL PRIMARY KEY,
                    email VARCHAR(180) NOT NULL,
                    roles JSON NOT NULL,
                    password VARCHAR(255) NOT NULL 
                )',
                self::FULL_NAME
            )
        );

        $this->addSql(sprintf('CREATE UNIQUE INDEX user__email ON %s(email)', self::FULL_NAME));
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP TABLE %s', self::FULL_NAME));
    }
}
