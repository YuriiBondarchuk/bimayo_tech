<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220710205146_order_offer_create extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'order_offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'CREATE TABLE %s(
                    id SERIAL NOT NULL PRIMARY KEY,
                    order_id INTEGER NOT NULL,
                    offer_id INTEGER NOT NULL,
                    offer_size VARCHAR(180),
                    offer_color VARCHAR(180),
                    quantity SMALLINT NOT NULL,
                    offer_price FLOAT NOT NULL,
                    total_price FLOAT NOT NULL,
                    --fk
                    FOREIGN KEY(offer_id) REFERENCES %s.offer(id) 
                        ON UPDATE CASCADE 
                        ON DELETE CASCADE,
                    FOREIGN KEY(order_id) REFERENCES %s.order(id) 
                        ON UPDATE CASCADE 
                        ON DELETE CASCADE
                )',
                self::FULL_NAME,
                self::SCHEMA_NAME,
                self::SCHEMA_NAME,
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP TABLE %s', self::FULL_NAME));
    }
}
