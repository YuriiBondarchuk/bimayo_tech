<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513160732_site_category_Create extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'category';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'CREATE TABLE %s(
                    id SERIAL NOT NULL PRIMARY KEY,
                    import_id INTEGER NOT NULL UNIQUE,
                    import_parent_id INTEGER NOT NULL,
                    name VARCHAR(255) NOT NULL UNIQUE,
                    need_translate BOOLEAN DEFAULT false
                )',
                self::FULL_NAME
            )
        );

        $this->addSql(
            sprintf(
                'CREATE UNIQUE INDEX %s__category_name_parent_id ON %s(name, import_parent_id)',
                self::TABLE_NAME,
                self::FULL_NAME,
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP TABLE %s', self::FULL_NAME));
    }
}
