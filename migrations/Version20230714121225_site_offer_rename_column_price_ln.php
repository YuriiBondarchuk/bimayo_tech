<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230714121225_site_offer_rename_column_price_ln extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s RENAME COLUMN priceln TO price_margin', self::FULL_NAME));
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s RENAME COLUMN price_margin TO priceln', self::FULL_NAME));
    }
}
