<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230522125524_site_offer_add_column_vendor extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s ADD COLUMN vendor_id INTEGER NOT NULL', self::FULL_NAME));
        $this->addSql(
            sprintf(
                'ALTER TABLE %s ADD FOREIGN KEY(vendor_id) REFERENCES %s.vendor(id)
                        ON UPDATE CASCADE
                        ON DELETE CASCADE',
                self::FULL_NAME,
                self::SCHEMA_NAME
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('ALTER TABLE %s DROP COLUMN vendor_id;', self::FULL_NAME));
    }
}
