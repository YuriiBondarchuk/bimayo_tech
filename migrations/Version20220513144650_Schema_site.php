<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513144650_Schema_site extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';

    public function up(Schema $schema): void
    {
        $this->addSql(sprintf('CREATE SCHEMA IF NOT EXISTS %s', self::SCHEMA_NAME));
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP SCHEMA IF EXISTS %s CASCADE', self::SCHEMA_NAME));
    }
}
