<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513160733_site_offer_create extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'CREATE TABLE %s(
                    id SERIAL NOT NULL PRIMARY KEY,
                    category_id INTEGER NOT NULL,
                    currency_name VARCHAR(255) NOT NULL,
                    available BOOLEAN,
                    selling_type VARCHAR(255),
                    url VARCHAR(255),
                    price FLOAT NOT NULL,
                    priceln FLOAT NOT NULL,
                    vendor_code VARCHAR(255),
                    delivery BOOLEAN,
                    name VARCHAR(255) NOT NULL,
                    description TEXT,
                    barcode BIGINT,
                    portal_category_id INTEGER,
                    vendor VARCHAR(255),
                    keywords TEXT,
                    is_recommended BOOLEAN,
                    need_translate BOOLEAN DEFAULT false
                )',
                self::FULL_NAME
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(sprintf('DROP TABLE %s', self::FULL_NAME));
    }
}
