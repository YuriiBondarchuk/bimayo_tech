<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221101105745_site_offer_alter_and_change_type_barcode_field extends AbstractMigration
{
    private const SCHEMA_NAME = 'site';
    private const TABLE_NAME = 'offer';
    private const FULL_NAME = self::SCHEMA_NAME . '.' . self::TABLE_NAME;

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'ALTER TABLE %s ALTER COLUMN barcode TYPE varchar (255);',
                self::FULL_NAME
            )
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                'ALTER TABLE %s ALTER COLUMN barcode TYPE bigint;',
                self::FULL_NAME
            )
        );
    }
}
