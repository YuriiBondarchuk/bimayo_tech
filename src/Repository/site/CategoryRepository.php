<?php

namespace App\Repository\site;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param Category $entity
     * @param bool $flush
     * @return void
     */
    public function add(Category $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param Category $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Category $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param array $importIds
     * @return array
     */
    public function getCategoriesByIds(array $importIds): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('c')
            ->from(Category::class, 'c')
            ->where('c.importId IN (:importIds)')
            ->setParameter('importIds', array_values($importIds))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $offer
     * @return Category
     */
    public function getCategoryByOffer(array $offer): Category
    {
        return $this->findOneBy([
            'importId' => $offer['categoryImportId']
        ]);
    }
}
