<?php

namespace App\Repository\site;

use App\Entity\site\Offer;
use App\Entity\site\OfferParam;
use App\Entity\site\OfferPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Offer>
 *
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    private const RANDOM_OFFERS_COUNT = 12;
    private const RANDOM_CATEGORY_COUNT = 1;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    /**
     * @param Offer $entity
     * @param bool $flush
     * @return void
     */
    public function add(Offer $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param Offer $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Offer $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return float|int|mixed|string
     */
    public function getRandomOffers(): mixed
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Offer::class, 'o')
            ->join(OfferPicture::class, 'p', Join::WITH, 'p.offer = o.id')
            ->orderBy('RANDOM()')
            ->setMaxResults(self::RANDOM_OFFERS_COUNT)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getRecommendedItems(): mixed
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Offer::class, 'o')
            ->join(OfferPicture::class, 'p', Join::WITH, 'p.offer = o.id')
            ->where('o.isRecommended = true')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $ids
     * @return QueryBuilder
     */
    public function getOffersByIds(array $ids): QueryBuilder
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->distinct('o.portalCategoryId')
            ->from(Offer::class, 'o')
            ->join(OfferPicture::class, 'p', Join::WITH, 'p.offer = o.id')
            ->where('o.categoryImportId IN (:category_ids)')
            ->setParameter('category_ids', array_values($ids));
    }

    /**
     * @param array $offer
     * @return array []
     */
    public function findIdenticalOffers(array $offer): array
    {
        $name = implode(' ', array_slice(explode(" ", $offer['name'], 3), 0, 2));
        $price = $offer['price'];
        $portalCategoryId = $offer['portalCategoryId'];
        $categoryImportId = $offer['categoryImportId'];

        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Offer::class, 'o')
            ->where('o.portalCategoryId = :portalCategoryId')
            ->andWhere('o.price = :price')
            ->andWhere('o.categoryImportId = :categoryImportId')
            ->andWhere('o.name LIKE :name')
            ->setParameters([
                'portalCategoryId' => $portalCategoryId,
                'price' => $price,
                'categoryImportId' => $categoryImportId,
                'name' => $name . '%'
            ])
            ->join(OfferParam::class, 'op', Join::WITH, 'op.offer = o.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $likeSql
     * @param int $countSuggestions
     * @return array
     */
    public function getSearchSuggestions(string $likeSql, int $countSuggestions): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Offer::class, 'o')
            ->join(OfferPicture::class, 'p', Join::WITH, 'p.offer = o.id')
            ->where('o.name LIKE :nameLower')
            ->orWhere('o.name LIKE :nameUp')
            ->setParameters([
                'nameLower' => '%' . strtolower($likeSql) . '%',
                'nameUp' => '%' . ucfirst($likeSql) . '%'
            ])
            ->setMaxResults($countSuggestions)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getRandomOffersByCategory(int $categoryImportId): mixed
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Offer::class, 'o')
            ->join(OfferPicture::class, 'p', Join::WITH, 'p.offer = o.id')
            ->orderBy('RANDOM()')
            ->where('o.categoryImportId = :categoryImportId')
            ->setParameter('categoryImportId', $categoryImportId)
            ->setMaxResults(self::RANDOM_OFFERS_COUNT)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getAllCategoryIds(): mixed
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('o.categoryImportId')
            ->distinct()
            ->from(Offer::class, 'o')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getRandomCategoryId(): mixed
    {
        $result = $this->getEntityManager()->createQueryBuilder()
            ->select('o.categoryImportId')
            ->from(Offer::class, 'o')
            ->orderBy('RANDOM()')
            ->setMaxResults(self::RANDOM_CATEGORY_COUNT)
            ->getQuery()
            ->getResult();

        return reset($result)['categoryImportId'] ?? 0;
    }

    /**
     * @param array $categoryIds
     * @return float|int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getPriceRangeByCategoryIds(array $categoryIds): mixed
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('min(o.priceMargin) as min, max(o.priceMargin) as max')
            ->from(Offer::class, 'o')
            ->where('o.categoryImportId IN (:category_ids)')
            ->setParameter('category_ids', array_values($categoryIds))
            ->getQuery()
            ->getSingleResult();
    }
}
