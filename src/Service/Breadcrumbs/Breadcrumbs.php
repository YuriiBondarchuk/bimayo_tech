<?php

declare(strict_types=1);

namespace App\Service\Breadcrumbs;

/**
 * Breadcrumbs class for building breadcrumbs
 */
class Breadcrumbs
{
    /** @var Breadcrumb[] $items */
    private array $items = [];

    /**
     * Add breadcrumb
     *
     * @return $this
     */
    public function add(Breadcrumb $breadcrumb): self
    {
        $this->items[] = $breadcrumb;

        return $this;
    }

    /**
     * Add breadcrumb
     *
     * @return Breadcrumbs []
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
