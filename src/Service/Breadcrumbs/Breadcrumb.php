<?php

declare(strict_types=1);

namespace App\Service\Breadcrumbs;

use JsonSerializable;

/**
 * Breadcrumb item class
 */
class Breadcrumb implements JsonSerializable
{
    /**
     * @var string $url
     */
    private string $url;

    /**
     * @var string $title
     */
    private string $title;

    /**
     * Breadcrumb constructor
     *
     * @param string      $title
     * @param string $url
     */
    public function __construct(string $title, string $url)
    {
        $this->url = $url;
        $this->title = $title;
    }

    /**
     * Getter for url
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Getter for title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }


    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        return [
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
        ];
    }
}
