<?php

declare(strict_types=1);

namespace App\Service\Api;

use Symfony\Component\HttpFoundation\Response;

/**
 * Default response for API layer.
 */
class ApiResponse extends Response
{
    public const STATUS_SUCCESS = 200;
}
