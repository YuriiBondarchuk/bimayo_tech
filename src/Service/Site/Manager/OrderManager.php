<?php

namespace App\Service\Site\Manager;

use App\Entity\site\Offer;
use App\Entity\site\Order;
use App\Entity\site\OrderOffer;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class OrderManager
{
    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param array $orderData
     * @return Order
     */
    public function createOrder(array $orderData): Order
    {
        $data = $orderData['data'];

        $order = new Order();
        $order->setStatus(Order::STATUS_NEW);
        $order->setUserName($data['name']);
        $order->setUserSurname($data['surName']);
        $order->setUserPhone($data['phone']);
        $order->setUserEmail($data['email']);
        $order->setDeliveryType((int)$data['deliveryType']);
        $order->setPaymentType((int)$data['paymentType']);
        $order->setPostCity($data['postCity']);
        $order->setPostOffice($data['postOffice']);
        $order->setComment($data['comment']);
        $order->setOrderTotalPrice($data['totalPrice']);

        $this->getOrderEntityManager()->persist($order);
        $this->getOrderEntityManager()->flush();

        $items = $orderData['items'];
        foreach ($items as $item) {
            $this->createOrderOffer($item, $order);
        }

        return $order;
    }

    /**
     * @param array $orderOfferData
     * @param Order $order
     * @return void
     */
    private function createOrderOffer(array $orderOfferData, Order $order): void
    {
        $offer = $this->getOfferEntityManager()->getRepository(Offer::class)->find($orderOfferData['id']);
        $orderOffer = new OrderOffer();
        $orderOffer->setOffer($offer);
        $orderOffer->setOrder($order);
        $orderOffer->setOfferColor($orderOfferData['color'] ?? '');
        $orderOffer->setOfferSize($orderOfferData['size'] ?? '');
        $orderOffer->setOfferPrice($orderOfferData['price']);
        $orderOffer->setQuantity($orderOfferData['count']);
        $orderOffer->setTotalPrice($orderOfferData['totalPrice']);

        $this->getOrderOfferEntityManager()->persist($orderOffer);
        $this->getOrderOfferEntityManager()->flush();
    }

    /**
     * @return ObjectManager
     */
    private function getOrderEntityManager(): ObjectManager
    {
        return $this->getManagerRegistry()->getManagerForClass(Order::class);
    }

    /**
     * @return ObjectManager
     */
    private function getOrderOfferEntityManager(): ObjectManager
    {
        return $this->getManagerRegistry()->getManagerForClass(OrderOffer::class);
    }

    /**
     * @return ObjectManager
     */
    private function getOfferEntityManager(): ObjectManager
    {
        return $this->getManagerRegistry()->getManagerForClass(Offer::class);
    }

    /**
     * @return ManagerRegistry
     */
    private function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }
}