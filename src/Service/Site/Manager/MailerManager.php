<?php

namespace App\Service\Site\Manager;

use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\BodyRenderer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Twig\Environment;
use Twig\Extra\CssInliner\CssInlinerExtension;
use Twig\Loader\FilesystemLoader;

class MailerManager
{
    private const SHOP_EMAIL = 'bimayo.shop@gmail.com';

    /**
     * @var Mailer
     */
    private Mailer $mailer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @param string $mailerDsn
     * @param LoggerInterface $siteLogger
     */
    public function __construct(string $mailerDsn, LoggerInterface $siteLogger)
    {
        $this->initMailer($mailerDsn);
        $this->siteLogger = $siteLogger;
    }

    /**
     * @param array $data
     * @param bool $isOrder
     * @return void
     */
    public function sendMail(array $data, bool $isOrder = true): void
    {
        $email = (new TemplatedEmail())
            ->from(self::SHOP_EMAIL)
            ->to(self::SHOP_EMAIL)
            ->addTo('ymbondarchuk@gmail.com');
        if ($isOrder) {
            $email->subject('Нове замовлення')
                ->addTo($data['data']['email'])
                ->htmlTemplate('order.html.twig')
                ->context([
                    'order' => $data['order'],
                    'items' => $data['items'],
                    'paymentType' => $data['paymentType'],
                    'deliveryType' => $data['deliveryType']
                ]);
        } else {
            $email->subject('Запитання від користувача')
                ->htmlTemplate('question.html.twig')
                ->context([
                    'userName' => $data['userName'],
                    'userEmail' => $data['userName'],
                    'userQuestion' => $data['userQuestion']
                ]);
        }

        $loader = new FilesystemLoader(['../templates/emails/', '../assets/styles/', '../assets/images/']);

        $twigEnv = new Environment($loader);
        $twigEnv->addExtension(new CssInlinerExtension());

        $twigBodyRenderer = new BodyRenderer($twigEnv);

        $twigBodyRenderer->render($email);

        try {
            $this->getMailer()->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->siteLogger->error($e->getMessage());

            throw new TransportException();
        }
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->mailer;
    }

    /**
     * @param string $mailerDsn
     * @return void
     */
    protected function initMailer(string $mailerDsn): void
    {
        $transport = Transport::fromDsn($mailerDsn);
        $this->mailer = new Mailer($transport);
    }
}