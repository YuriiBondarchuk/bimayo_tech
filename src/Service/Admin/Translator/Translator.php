<?php

namespace App\Service\Admin\Translator;

use Stichoza\GoogleTranslate\GoogleTranslate;

class Translator
{
    /**
     * @var GoogleTranslate $translator
     */
    private GoogleTranslate $translator;

    public function __construct()
    {
        $this->translator = new GoogleTranslate();

        $this->translator->setSource('ru');
        $this->translator->setTarget('uk');
    }

    /**
     * @return GoogleTranslate
     */
    public function getTranslator(): GoogleTranslate
    {
        return $this->translator;
    }
}