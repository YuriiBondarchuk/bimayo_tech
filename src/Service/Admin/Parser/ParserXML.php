<?php

namespace App\Service\Admin\Parser;

use App\Service\Admin\Builder\Base\GeneralBuilder;
use Doctrine\Persistence\ManagerRegistry;
use DOMElement;
use DOMNodeList;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ParserXML
{
    public const REQUIRED_EXTENSION = 'xml';

    /**
     * @var Crawler
     */
    protected Crawler $crawler;

    /**
     * @var CatalogXmlDto
     */
    private CatalogXmlDto $xmlDto;

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $managerRegistry;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * ParserXML constructor
     */
    public function __construct(UploadedFile $uploadedFile, ManagerRegistry $managerRegistry, LoggerInterface $siteLogger)
    {
        $this->siteLogger = $siteLogger;
        $this->managerRegistry = $managerRegistry;
        $this->xmlDto = new CatalogXmlDto();
        $this->crawler = new Crawler();
        $this->crawler->addXmlContent(file_get_contents($uploadedFile));
    }

    /**
     * @param bool $needTranslate
     * @throws Exception
     */
    public function parse(bool $needTranslate): void
    {
        $this->parseVendor();
        $this->parseCurrencies();
        $this->parseCategory();
        $this->parseOffers();
        $this->getSiteLogger()->debug(round(memory_get_usage() / 1024 / 1024, 2) . ' MB' . PHP_EOL);
        $this->insertParseDataToDb($needTranslate);


    }

    /**
     * @return LoggerInterface
     */
    public function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }

    /**
     * @return void
     */
    private function parseCategory(): void
    {
        $categories = $this->crawler->filter('categories > category');
        /** @var DOMElement $category */
        foreach ($categories as $category) {
            $this->xmlDto->setCategory([
                'name' => $category->nodeValue,
                'import_id' => $category->getAttribute('id'),
                'import_parent_id' => $category->getAttribute('parentId'),
            ]);
        }
    }

    /**
     * @return void
     */
    private function parseCurrencies(): void
    {
        $attrName = 'id';
        $attrRate = 'rate';

        $currencies = $this->crawler->filter('currency');
        /** @var DOMElement $currency */
        foreach ($currencies as $currency) {
            $this->xmlDto->setCurrencies([
                'name' => $currency->getAttribute($attrName),
                $attrRate => $currency->getAttribute($attrRate)
            ]);
        }
    }

    /**
     * @return void
     */
    private function parseOffers()
    {
        $offers = $this->crawler->filter('offer');
        /** @var DOMElement $offer */
        foreach ($offers as $offer) {
            $finedOffer = [];
            $finedOffer['available'] = $offer->getAttribute('available');
            $finedOffer['selling_type'] = $offer->getAttribute('selling_type');

            $this->parsePicture($offer->getElementsByTagName('picture'), $finedOffer);
            $this->parseOfferParam($offer->getElementsByTagName('param'), $finedOffer);

            foreach ($offer->childNodes as $attribute) {
                if ($attribute->nodeName !== 'picture' && $attribute->nodeName !== 'param') {
                    $finedOffer[$attribute->nodeName] = $attribute->nodeValue;
                }
            }
            yield $finedOffer;
        }
    }

    /**
     * @param DOMNodeList $params
     * @param $offer
     * @return void
     */
    private function parseOfferParam(DOMNodeList $params, &$offer): void
    {
        $finedParam = [];
        foreach ($params as $param) {
            $finedParam[$param->getAttribute('name')] = $param->nodeValue;
        }
        $offer['params'] = $finedParam;
    }

    /**
     * @param DOMNodeList $pictures
     * @param $offer
     * @return void
     */
    private function parsePicture(DOMNodeList $pictures, &$offer): void
    {
        $finedPicture = [];
        foreach ($pictures as $picture) {
            $finedPicture[] = $picture->nodeValue;
        }
        $offer['pictures'] = $finedPicture;
    }

    /**
     * @return void
     */
    private function parseVendor(): void
    {
        $vendor = [];
        $vendor['url'] = $this->crawler->filter('shop')->filter('url')->text();
        $vendor['name'] = $this->crawler->filter('shop')->filter('name')->text();
        $vendor['company'] = $this->crawler->filter('shop')->filter('company')->text();

        $this->xmlDto->setVendor($vendor);
    }

    /**
     * @param bool $needTranslate
     * @return void
     * @throws JsonException
     * @throws Exception
     */
    private function insertParseDataToDb(bool $needTranslate): void
    {
        $generalBuilder = new GeneralBuilder($this->managerRegistry, $this->xmlDto, $this->getSiteLogger());
        $generalBuilder->insertVendor();
        $generalBuilder->insertCategory($needTranslate);
        $generalBuilder->insertCurrency();
        $generalBuilder->insertOffer($needTranslate, $this->parseOffers());
    }
}