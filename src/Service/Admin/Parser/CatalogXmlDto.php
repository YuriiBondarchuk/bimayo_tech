<?php

namespace App\Service\Admin\Parser;

class CatalogXmlDto
{
    /**
     * @var array
     */
    private array $vendor;
    /**
     * @var array
     */
    private array $category;
    /**
     * @var array
     */
    private array $currencies;
    /**
     * @var array
     */
    private array $offers;

    /**
     * @return array
     */
    public function getVendor(): array
    {
        return $this->vendor;
    }

    /**
     * @param array $vendor
     */
    public function setVendor(array $vendor): void
    {
        $this->vendor = $vendor;
    }

    /**
     * @return array
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    /**
     * @param array $category
     */
    public function setCategory(array $category): void
    {
        $this->category [] = $category;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @param array $currencies
     */
    public function setCurrencies(array $currencies): void
    {
        $this->currencies[] = $currencies;
    }

    /**
     * @return array
     */
    public function getOffers(): array
    {
        return $this->offers;
    }

    /**
     * @param array $offer
     */
    public function setOffers(array $offer): void
    {
        $this->offers[] = $offer;
    }
}