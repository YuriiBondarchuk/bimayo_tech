<?php

namespace App\Service\Admin\Manager;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Repository\site\CategoryRepository;
use App\Repository\site\OfferRepository;
use ArrayObject;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class CategoryManager
{
    /**
     * @var ManagerRegistry $managerRegistry
     */
    private ManagerRegistry $managerRegistry;

    /**
     * @var SerializerInterface $serializer
     */
    private SerializerInterface $serializer;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param SerializerInterface $serializer
     */
    public function __construct(ManagerRegistry $managerRegistry, SerializerInterface $serializer)
    {
        $this->managerRegistry = $managerRegistry;
        $this->serializer = $serializer;
    }

    /**
     * @throws ExceptionInterface
     */
    public function buildCategoryTree(): array
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->managerRegistry->getRepository(Category::class);

        $mainCategories = $this->serializer->normalize(
            array_reverse($this->getMainCategories($categoryRepository)),
            null,
            [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offers']]
        );

        $allCategories = $this->serializer->normalize(
            $this->getAllCategories($categoryRepository),
            null,
            [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offers']]
        );

        $mainCategories = array_reverse($mainCategories);

        foreach ($mainCategories as $key => $mainCategory) {
            $this->createCategoryTree($allCategories, $mainCategories[$key], 0);
        }

        $this->removeEmptyChildrenCategory($mainCategories);
        $this->removeEmptyParentCategory($mainCategories);

        return $mainCategories;
    }

    /**
     * @return array
     */
    public function getAllCategoryIds(): array
    {
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->managerRegistry->getRepository(Offer::class);

        $categoryIds = $offerRepository->getAllCategoryIds();

        return $this->selectAllCategoryIds($categoryIds);
    }

    /**
     * @param string $url
     * @return int
     */
    public function getCategoryImportIdsByUrl(string $url): array
    {
        $foundCategoryIds = [];

        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->managerRegistry->getRepository(Category::class);
        $allCategories = $this->getAllCategories($categoryRepository);

        $parentCategory = $categoryRepository->findBy(['url' => $url]);
        $foundCategories =[];
        $this->foundAllSubCategories($allCategories, $parentCategory, $foundCategories);

        foreach ($foundCategories as $category) {
            $foundCategoryIds[] = $category->getImportId();
        }

        return $foundCategoryIds;
    }

    /**
     * @param CategoryRepository $repository
     * @return Category[]
     */
    private function getMainCategories(CategoryRepository $repository, string $mainCategoryName = ''): array
    {
        if (empty($mainCategoryName)) {
            return $repository->findBy(
                ['importParentId' => 0],
                ['orderNumber' => 'ASC']
            );
        }

        return $repository->findBy(
            ['importParentId' => 0, 'name' => $mainCategoryName],
            ['orderNumber' => 'ASC']
        );
    }

    /**
     * @param CategoryRepository $repository
     * @return array
     */
    private function getAllCategories(CategoryRepository $repository): array
    {
        return $repository->findAll();
    }

//    /**
//     * @param array $objects
//     * @return string|int|bool|ArrayObject|array|float|null
//     * @throws ExceptionInterface
//     */
//    private function convertToArray(array $objects, ?string $format=null, ): string|int|bool|ArrayObject|array|null|float
//    {
//        return $this->serializer->normalize($objects, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offers']]);
//    }

    /**
     * @param $categories
     * @param $tries
     * @param $level
     * @return void
     */
    private function createCategoryTree($categories, &$tries, $level): void
    {
        foreach ($categories as $category) {
            if ($category['importParentId'] === $tries['importId']) {
                $offerCount = $this->isExistOffersForCategory($category);

                if (!array_key_exists('children', $tries)) {
                    $tries['children'] = [];
                }

                $tries['level'] = $level;
                $tries['children'][$category['id']] = $category;
                $tries['children'][$category['id']]['count'] = !empty($offerCount) ? count($offerCount) : 0;

                $this->createCategoryTree($categories, $tries['children'][$category['id']], $level + 1);
            } else {
                $tries['level'] = $level;
            }
        }
    }

    /**
     * @param $categories
     * @return array
     */
    private function selectAllCategoryIds(&$categories): array
    {
        $categoryIds = [];
        foreach ($categories as $category) {
            $categoryIds[] = $category['categoryImportId'];
        }

        return $categoryIds;
    }

    /**
     * @param array $category
     * @return array
     */
    private function isExistOffersForCategory(array $category): array
    {
        $offerRepository = $this->managerRegistry->getManagerForClass(Offer::class)->getRepository(Offer::class);
        return $offerRepository->findBy(['categoryImportId' => $category['importId']]);
    }

    /**
     * @param $mainCategories
     * @return void
     */
    private function removeEmptyChildrenCategory(&$mainCategories): void
    {
        foreach ($mainCategories as $key => $mainCategory) {
            if (array_key_exists('children', $mainCategory)) {
                $this->removeEmptyChildrenCategory($mainCategories[$key]['children']);
            } elseif (isset($mainCategory['count']) && $mainCategory['count'] === 0) {
                unset($mainCategories[$key]);
            }
        }
    }

    /**
     * @param $mainCategories
     * @return void
     */
    private function removeEmptyParentCategory(&$mainCategories): void
    {
        foreach ($mainCategories as $mainKey => $mainCategory) {
            if (isset($mainCategory['children'])) {
                foreach ($mainCategory['children'] as $key => $subMainCategory) {
                    if (isset($subMainCategory['count']) && $subMainCategory['count'] === 0 && empty($subMainCategory['children'])) {
                        unset($mainCategories[$mainKey]['children'][$key]);
                    }
                }
            }
        }
    }

    /**
     * @param Category[] $allCategories
     * @param Category[] $parentCategories
     *
     * @return array
     */
    public function foundAllSubCategories(array $allCategories, array $parentCategories, &$test): void
    {
        foreach($parentCategories as $parentCategory) {
            $tree = [];
            foreach ($allCategories as $category) {
                if ($parentCategory->getImportId()=== $category->getImportParentId()) {
                    $tree[] = $category;
                }
            }

            if ([] === $tree) {
                $test[] = $parentCategory;
            }else{
                $this->foundAllSubcategories($allCategories, $tree, $test);
                unset($tree);
            }
        }
    }
}