<?php

namespace App\Service\Admin\Builder;

use App\Entity\site\Vendor;
use App\Service\Admin\Builder\Base\BuilderBase;
use Doctrine\Persistence\ObjectManager;
use Exception;

class VendorBuilder extends BuilderBase
{
    /**
     * @throws Exception
     */
    public function insertToDb(): void
    {
        $manager = $this->getManagerRegistry()->getManagerForClass(Vendor::class);

        $vendor = $this->getXmlDto()->getVendor();

        if (!$this->checkIfUnique($vendor['name'], $manager)) {
            return;
        }

        $vendorEntity = new Vendor();;
        $vendorEntity->setName($vendor['name']);
        $vendorEntity->setCompany($vendor['company']);
        $vendorEntity->setUrl($vendor['url']);

        $manager->persist($vendorEntity);

        $manager->flush();
        $manager->clear();
    }

    /**
     * @param mixed $fieldValue
     * @param ObjectManager $manager
     * @return bool
     */
    private function checkIfUnique(mixed $fieldValue, ObjectManager $manager): bool
    {
        $categoryRepository = $manager->getRepository(Vendor::class);
        $vendors = $categoryRepository->findBy(['name' => $fieldValue]);

        return empty($vendors);
    }
}