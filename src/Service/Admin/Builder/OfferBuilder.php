<?php

namespace App\Service\Admin\Builder;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Entity\site\OfferParam;
use App\Entity\site\OfferPicture;
use App\Entity\site\Vendor;
use App\Service\Admin\Builder\Base\GeneralBuilder;
use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use JsonException;

class OfferBuilder extends GeneralBuilder
{
    /**
     * @var ObjectManager
     */
    private ObjectManager $manager;

    /**
     * @param bool $needTranslate
     * @param $offers
     * @return void
     */
    public function insertToDb(bool $needTranslate, iterable $offers): void
    {
        $this->manager = $this->getManagerRegistry()->getManager();
        $this->getManagerRegistry()->getConnection()->getConfiguration()->setSQLLogger(null);

        $slugify = new Slugify();

        /** @var Vendor $vendor */
        $vendor = $this->manager->getRepository(Vendor::class)
            ->findOneBy(['name' => $this->getXmlDto()->getVendor()['name']]);

        try {
            $batchSize = 500;
            $i = 1;

            foreach ($offers as $offer) {
                if ((int)$offer['price'] === 0) {
                    continue;
                }

                if (!array_key_exists('vendorCode', $offer)) {
                    $offer['vendorCode'] = $offer['code'] ?? null;
                }

                $portalCategoryId = null;
                if (isset($offer['portal_category_id'])) {
                    $portalCategoryId = (int)$offer['portal_category_id'];
                }

//                if (!isset($offer['url'])) {
                $offerUrl = $slugify->slugify($offer['name']);
//                } else {
//                    $needTrim = str_contains($offer['url'], 'https://');
//
//                    $offerUrl = $needTrim ? $this->changeOfferUrl($offer['url']) : $offer['url'];
//                }

                $priceMargin = $offer['priceln'] ?? $offer['price_drop'] ?? $offer['price'];

                $category = $this->manager->getRepository(Category::class)
                    ->findOneBy(['importId' => $offer['categoryId']]);

                if (!$category) {
                    continue;
                }

                $offerEntity = new Offer();
                $offerEntity->setCategory($category);
                $offerEntity->setCategoryImportId((int)$offer['categoryId']);
                $offerEntity->setCurrencyName($offer['currencyId']);
                $offerEntity->setName($offer['name']);
                $offerEntity->setAvailable((bool)$offer['available']);
                $offerEntity->setSellingType($offer['selling_type']);
                $offerEntity->setUrl($offerUrl);
                $offerEntity->setPrice((float)$offer['price']);
                $offerEntity->setPriceMargin($priceMargin);
                $offerEntity->setVendorCode((string)$offer['vendorCode']);
                $offerEntity->setDelivery($offer['delivery'] ?? null);
                $offerEntity->setDescription($offer['description']);
                $offerEntity->setBarcode($offer['barcode'] ?? null);
                $offerEntity->setPortalCategoryId($portalCategoryId);
                $offerEntity->setVendor($vendor);
                $offerEntity->setKeywords($offer['keywords'] ?? '');
                $offerEntity->setNeedTranslate($needTranslate);
                $offerEntity->setOldPrice($offer['oldprice'] ?? null);

                $this->manager->persist($offerEntity);

                $tempObjets[] = $offerEntity;

                $this->insertOfferParamsToDb($offerEntity, $offer['params']);
                $this->insertOfferPicturesToDb($offerEntity, $offer['pictures']);

                if (($i % $batchSize) === 0) {
                    $this->manager->flush();
                    $this->manager->clear();

                    // IMPORTANT - clean entities
                    foreach($tempObjets as $tempObject) {
                        $this->manager->detach($tempObject);
                    }

                    $tempObjets = null;
                    gc_enable();
                    gc_collect_cycles();

                    $vendor = $this->manager->getReference(Vendor::class, $vendor->getId());
                }

                ++$i;
            }
            $this->getSiteLogger()->debug(round(memory_get_usage() / 1024 / 1024, 2) . ' MB' . PHP_EOL) ;
            $this->manager->flush();
            $this->manager->clear();

        } catch (\Throwable $e) {
            $this->getSiteLogger()->error($e->getMessage());
        }
    }

    /**
     * @param Offer $offer
     * @param array $offerParams
     * @return void
     * @throws JsonException
     */
    private function insertOfferParamsToDb(Offer $offer, array $offerParams): void
    {
        $offerParamEntity = new OfferParam();
        $offerParamEntity->setOffer($offer);
        $offerParamEntity->setParams(json_encode($offerParams, JSON_THROW_ON_ERROR));

        $this->manager->persist($offerParamEntity);
    }

    /**
     * @param Offer $offer
     * @param array $offerPictures
     * @return void
     * @throws JsonException
     */
    private function insertOfferPicturesToDb(Offer $offer, array $offerPictures): void
    {
        foreach ($offerPictures as $key => $offerPicture) {
            $offerPictures[$key] = str_replace('http://', 'https://', $offerPicture);
        }

        $offerPictureEntity = new OfferPicture();
        $offerPictureEntity->setOffer($offer);
        $offerPictureEntity->setPictures(json_encode($offerPictures, JSON_THROW_ON_ERROR));

        $this->manager->persist($offerPictureEntity);
    }

    /**
     * @param string $url
     * @return string
     *
     * TODO: temporary not used
     */
    private function changeOfferUrl(string $url): string
    {
        $importSiteUrl = $this->getXmlDto()->getVendor()['url'];

        return trim(substr($url, strlen($importSiteUrl)),'/');
    }
}