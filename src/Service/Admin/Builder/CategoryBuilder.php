<?php

namespace App\Service\Admin\Builder;

use App\Entity\site\Category;
use App\Service\Admin\Builder\Base\GeneralBuilder;
use App\Service\Admin\Translator\Translator;
use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use ErrorException;

class CategoryBuilder extends GeneralBuilder
{
    /**
     * @param bool $needTranslate
     * @throws ErrorException
     */
    public function insertToDb(bool $needTranslate): array
    {
        $insertedCategories = [];
        $manager = $this->getManagerRegistry()->getManager();

        $translator = new Translator();
        $categories = $this->getXmlDto()->getCategory();
        foreach ($categories as $category) {
            $categoryEntity = new Category();
            $categoryEntity->setImportId((int)$category['import_id']);
            $categoryEntity->setImportParentId((int)$category['import_parent_id']);

            if ($needTranslate) {
                $categoryName = $translator->getTranslator()->translate($category['name']);
            } else {
                $categoryName = $category['name'];
            }

            if (!$this->checkIfUnique($categoryName, $manager)) {
                continue;
            }

            $categoryEntity->setName($categoryName);

            $slugify = new Slugify();
            $categoryEntity->setUrl($slugify->slugify($category['name']));

            $manager->persist($categoryEntity);

            $manager->flush();
            $manager->clear();

            $insertedCategories[$category['import_id']] = $categoryEntity;
        }

        return $insertedCategories;
    }

    /**
     * @param mixed $fieldValue
     * @param ObjectManager $manager
     * @return bool
     */
    private function checkIfUnique(mixed $fieldValue, ObjectManager $manager): bool
    {
        $categoryRepository = $manager->getRepository(Category::class);
        $categories = $categoryRepository->findBy(['name' => $fieldValue]);

        return empty($categories);
    }
}