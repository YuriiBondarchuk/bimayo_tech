<?php

namespace App\Service\Admin\Builder;

use App\Entity\site\Currencies;
use App\Service\Admin\Builder\Base\GeneralBuilder;

class CurrencyBuilder extends GeneralBuilder
{
    /**
     * @return void
     */
    public function insertToDb(): void
    {
        $manager = $this->getManagerRegistry()->getManager();

        $currencies = $this->getXmlDto()->getCurrencies();
        foreach ($currencies as $currency) {
            $currencyEntity = new Currencies();
            $currencyEntity->setName($currency['name']);
            $currencyEntity->setRate(is_numeric($currency['rate']) ? $currency['rate'] : 0);

            $manager->persist($currencyEntity);
        }

        $manager->flush();
        $manager->clear();
    }
}