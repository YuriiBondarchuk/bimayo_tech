<?php

namespace App\Service\Admin\Builder\Base;

use App\Service\Admin\Parser\CatalogXmlDto;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class BuilderBase
{
    /**
     * @var CatalogXmlDto
     */
    private CatalogXmlDto $xmlDto;

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $managerRegistry;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param CatalogXmlDto $xmlDto
     * @param LoggerInterface $siteLogger
     */
    public function __construct(ManagerRegistry $managerRegistry, CatalogXmlDto $xmlDto, LoggerInterface $siteLogger)
    {
        $this->managerRegistry = $managerRegistry;
        $this->xmlDto = $xmlDto;
        $this->siteLogger = $siteLogger;
    }

    /**
     * @return ManagerRegistry
     */
    protected function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }

    /**
     * @return CatalogXmlDto
     */
    public function getXmlDto(): CatalogXmlDto
    {
        return $this->xmlDto;
    }

    /**
     * @return LoggerInterface
     */
    public function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }

}