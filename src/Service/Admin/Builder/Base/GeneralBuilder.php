<?php

namespace App\Service\Admin\Builder\Base;

use App\Entity\site\Category;
use App\Service\Admin\Builder\CategoryBuilder;
use App\Service\Admin\Builder\CurrencyBuilder;
use App\Service\Admin\Builder\OfferBuilder;
use App\Service\Admin\Builder\VendorBuilder;
use Exception;
use JsonException;


class GeneralBuilder extends BuilderBase
{
    /**
     * @var Category []
     */
    protected array $insertedCategories = [];

    /**
     * @return void
     * @throws Exception
     */
    public function insertVendor(): void
    {
        $vendor = new VendorBuilder($this->getManagerRegistry(), $this->getXmlDto(), $this->getSiteLogger());
        $vendor->insertToDb();
    }

    /**
     * @param bool $needTranslate
     * @return void
     * @throws Exception
     */
    public function insertCategory(bool $needTranslate): array
    {
        $category = new CategoryBuilder($this->getManagerRegistry(), $this->getXmlDto(), $this->getSiteLogger());
        return $category->insertToDb($needTranslate);
    }

    /**
     * @return void
     */
    public function insertCurrency(): void
    {
        $currency = new CurrencyBuilder($this->getManagerRegistry(), $this->getXmlDto(), $this->getSiteLogger());
        $currency->insertToDb();
    }

    /**
     * @param bool $needTranslate
     * @param array $offers
     *
     * @return void
     */
    public function insertOffer(bool $needTranslate, iterable $offers): void
    {
        $offer = new OfferBuilder($this->getManagerRegistry(), $this->getXmlDto(), $this->getSiteLogger());
        $offer->insertToDb($needTranslate, $offers);
    }
}