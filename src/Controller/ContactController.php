<?php

namespace App\Controller;

use App\Service\Breadcrumbs\Breadcrumb;
use App\Service\Breadcrumbs\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractControllerBase implements HasBreadcrumbsInterface
{
    use HasBreadcrumbsTrait;

    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request): Response
    {
        return $this->render('pages/contact.html.twig',
            [
                'pageName' => "Контакти",
                'url' => $request->getUri(),
                'breadcrumbs'=> $this->configureBreadcrumbs()
            ]);
    }

    /**
     * @return array
     */
    public function configureBreadcrumbs(): array
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs->add(new Breadcrumb('🏠', '/'));
        $breadcrumbs->add(new Breadcrumb('Контакти', $this->getCurrentUri()));

        return $this->convertBreadcrumbsToArray($breadcrumbs);
    }
}
