<?php

namespace App\Controller;

use App\Service\Breadcrumbs\Breadcrumbs;

interface HasBreadcrumbsInterface
{
    /**
     * @return array
     */
    public function configureBreadcrumbs(): array;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @return array
     */
    public function convertBreadcrumbsToArray(Breadcrumbs $breadcrumbs): array;
}