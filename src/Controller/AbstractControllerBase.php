<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

class AbstractControllerBase extends AbstractController
{
    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @var string
     */
    private string $currentUri = '';

    /**
     * @param ManagerRegistry $doctrine
     * @param SerializerInterface $serializer
     * @param LoggerInterface $siteLogger
     */
    public function __construct(
        ManagerRegistry $doctrine,
        SerializerInterface $serializer,
        LoggerInterface $siteLogger,
        RequestStack $requestStack
    )
    {
        $this->setDoctrine($doctrine);
        $this->setSerializer($serializer);
        $this->setSiteLogger($siteLogger);
        $this->setCurrentUri($requestStack->getCurrentRequest()->getUri());
    }

    /**
     * @return ManagerRegistry
     */
    protected function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    private function setDoctrine(ManagerRegistry $doctrine): void
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return SerializerInterface
     */
    protected function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @param SerializerInterface $serializer
     */
    protected function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @return LoggerInterface
     */
    protected function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }

    /**
     * @param LoggerInterface $siteLogger
     */
    protected function setSiteLogger(LoggerInterface $siteLogger): void
    {
        $this->siteLogger = $siteLogger;
    }

    /**
     * @return string
     */
    public function getCurrentUri(): string
    {
        return $this->currentUri;
    }

    /**
     * @param string $currentUri
     */
    public function setCurrentUri(string $currentUri): void
    {
        $this->currentUri = $currentUri;
    }
}