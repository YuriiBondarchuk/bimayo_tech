<?php

namespace App\Controller;

use App\Service\Breadcrumbs\Breadcrumb;
use App\Service\Breadcrumbs\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractControllerBase implements HasBreadcrumbsInterface
{
    use HasBreadcrumbsTrait;

    /**
     */
    #[Route('/about_us', name: 'app_about_us')]
    public function index(Request $request): Response
    {
        return $this->render(
            'pages/about_us.html.twig',
            [
                'pageName' => "Про Нас",
                'url' => $request->getUri(),
                'breadcrumbs' => $this->configureBreadcrumbs()
            ]
        );
    }

    /**
     * @return array
     */
    public function configureBreadcrumbs(): array
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs->add(new Breadcrumb('🏠', '/'));
        $breadcrumbs->add(new Breadcrumb('Про нас', $this->getCurrentUri()));

        return $this->convertBreadcrumbsToArray($breadcrumbs);
    }
}
