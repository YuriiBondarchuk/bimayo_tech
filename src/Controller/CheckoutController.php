<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends AbstractControllerBase
{
    #[Route('/checkout', name: 'app_checkout', methods: ["GET"])]
    public function index(Request $request): Response
    {
        return $this->render('pages/checkout.html.twig',
            [
                'pageName' => "Кошик",
                'url' => $request->getUri()
            ]
        );
    }
}
