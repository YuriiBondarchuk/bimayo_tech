<?php

namespace App\Controller;

use App\Entity\site\Category;
use App\Repository\site\CategoryRepository;
use App\Service\Breadcrumbs\Breadcrumbs;

trait HasBreadcrumbsTrait
{
    /**
     * @return array
     */
    public function convertBreadcrumbsToArray(Breadcrumbs $breadcrumbs): array
    {
        return array_map(
            static function ($breadcrumb): array {
                return $breadcrumb->jsonSerialize();
            },
            $breadcrumbs->getItems()
        );
    }

    /**
     * @param Category $childCategory
     * @param CategoryRepository $categoryRepository
     * @return array
     */
    public function buildCategoriesTreeByCategoryId(
        Category $childCategory,
        CategoryRepository $categoryRepository
    ): void {
        $allCategories = $categoryRepository->findAll();
        $this->childCategoryTreeBuild($allCategories, $childCategory, $this->parentCategories);
    }

    /**
     * @param $allCategories
     * @param $categoryToProduct
     * @param $tree
     * @return void
     */
    public function childCategoryTreeBuild($allCategories, $categoryToProduct, &$tree): void
    {
        foreach ($allCategories as $category) {
            if ($categoryToProduct->getImportId() === $category->getImportId()) {
                $tree[] = $category;
                if ($category->getImportParentId() !== 0) {
                    $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
                    $newCategory = $categoryRepository->findBy(['importId' => $category->getImportParentId()]);
                    $this->childCategoryTreeBuild($allCategories, $newCategory[0], $tree);
                }
            }
        }
    }
}