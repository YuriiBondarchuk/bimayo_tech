<?php

namespace App\Controller;

use App\Service\Breadcrumbs\Breadcrumb;
use App\Service\Breadcrumbs\Breadcrumbs;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractControllerBase implements HasBreadcrumbsInterface
{
    use HasBreadcrumbsTrait;

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render(
            'pages/home.html.twig',
            [
                'pageName' => "Головна сторінка",
                'url' => $this->getCurrentUri(),
                'breadcrumbs' => $this->configureBreadcrumbs()
            ]
        );
    }

    /**
     * @return array
     */
    public function configureBreadcrumbs(): array
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs->add(new Breadcrumb('Головна', $this->getCurrentUri()));

        return $this->convertBreadcrumbsToArray($breadcrumbs);
    }
}
