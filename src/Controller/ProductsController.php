<?php

namespace App\Controller;

use App\Entity\site\Category;
use App\Repository\site\CategoryRepository;
use App\Service\Breadcrumbs\Breadcrumb;
use App\Service\Breadcrumbs\Breadcrumbs;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractControllerBase implements HasBreadcrumbsInterface
{
    use HasBreadcrumbsTrait;

    /**
     * @var array
     */
    private array $parentCategories = [];

    #[Route(['/products/{url}', '/products'], name: 'app_products', methods: ["GET"])]
    public function index(?Category $category): Response
    {
        if (null !== $category) {
            /** @var CategoryRepository $categoryRepository */
            $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
            $this->buildCategoriesTreeByCategoryId($category, $categoryRepository);
        }

        return $this->render(
            'pages/products.html.twig',
            [
                'pageName' => "Посуд та аксесуари, товари для дому, електроніка, побутова техніка",
                'url' => $this->getCurrentUri(),
                'breadcrumbs' => $this->configureBreadcrumbs()
            ]
        );
    }

    /**
     * @return array
     */
    public function configureBreadcrumbs(): array
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs->add(new Breadcrumb('🏠', '/'));

        if ([] !== $this->parentCategories) {
            $parentCategories = array_reverse($this->parentCategories);
            /** @var Category $parentCategory */
            foreach ($parentCategories as $parentCategory) {
                $breadcrumbs->add(new Breadcrumb($parentCategory->getName(), '/products/' . $parentCategory->getUrl()));
            }
        } else {
            $breadcrumbs->add(new Breadcrumb('Всі товари', $this->getCurrentUri()));
        }

        return $this->convertBreadcrumbsToArray($breadcrumbs);
    }
}
