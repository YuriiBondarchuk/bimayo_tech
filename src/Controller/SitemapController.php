<?php

namespace App\Controller;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Repository\site\CategoryRepository;
use App\Repository\site\OfferRepository;
use App\Utils\SystemHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    private const SITEMAP_BATCH = 25;
    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->setDoctrine($doctrine);
    }

    #[Route('/sitemap.xml', name: 'sitemap')]
    public function sitemap(): Response
    {
        $categoryIds = $this->getOfferRepository()->getAllCategoryIds();
        $countCategories = count($categoryIds);
        $countSitemaps = 0;
        $siteUrl = SystemHelper::getProjectBaseUrl();

        for ($i = 0; $i < $countCategories; $i++) {
            if ($i % self::SITEMAP_BATCH == 0) {
                $countSitemaps++;
            }
        }

        $response = new Response(
            $this->renderView('sitemap/sitemaps.html.twig', [
                'countSitemaps' => $countSitemaps,
                'siteUrl' => $siteUrl
            ]),
            200
        );
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

    #[Route('/sitemap-{num}.xml', name: 'sitemaps', requirements: ['num' => '\d+'])]
    public function sitemaps(int $num): Response
    {
        $categoryIds = $this->getOfferRepository()->getAllCategoryIds();

        $categoryUrls = [];
        $offerUrls = [];

        $siteUrl = SystemHelper::getProjectBaseUrl();
        $offers = [];
        $partCategoryIds = array_slice($categoryIds, ($num - 1) * self::SITEMAP_BATCH, self::SITEMAP_BATCH);
        $categories = $this->getCategoryRepository()->getCategoriesByIds($partCategoryIds);
        foreach ($categories as $category) {
            $categoryUrls [] = $siteUrl . '/products/' . $category->getUrl();
            $offers = array_merge($offers, $this->getOfferRepository()->findBy(['categoryImportId' => $category->getImportId()]));
        }
        unset($categories);

        foreach ($offers as $offer) {
            $offerUrls [] = $siteUrl . '/product/' . $offer->getUrl();
        }
        $exampleOffer = array_shift($offers);
        unset($offers);

        $sitemapUrls = array_merge($categoryUrls, $offerUrls);

        $response = new Response(
            $this->renderView('sitemap/sitemaps.html.twig', [
                'sitemapUrls' => $sitemapUrls,
                'lastmod' => $exampleOffer->getTins()->format('Y-m-d'),
// now this attributes is ignored. https://developers.google.com/search/docs/advanced/sitemaps/build-sitemap?hl=ru
//                'changefreq' => 'weekly',
//                'priority' => '1',
            ]),
            200
        );
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

    /**
     * @return ManagerRegistry
     */
    private function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    private function setDoctrine(ManagerRegistry $doctrine): void
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return OfferRepository
     */
    public function getOfferRepository(): OfferRepository
    {
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getRepository(Offer::class);
        return $offerRepository;
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository(): CategoryRepository
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);

        return $categoryRepository;
    }

}