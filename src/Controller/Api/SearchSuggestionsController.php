<?php

namespace App\Controller\Api;

use App\Entity\site\Offer;
use App\Repository\site\OfferRepository;
use JsonException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class SearchSuggestionsController extends ApiControllerBase
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|JsonException
     */
    #[Route('/api/searchSuggestions', name: 'app_api_search_suggestion', methods: ['POST'])]
    public function features(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if ($data === null || !array_key_exists('countSuggestions', $data)) {
            throw new BadRequestHttpException('Invalid JSON');
        }

        $countSuggestions = $data['countSuggestions'];
        $like = $data['likeSql'];

        $suggestions = $this->getSuggestions($like, $countSuggestions);

        $suggestions = json_decode(
            $this->getSerializer()->serialize(
                $suggestions, 'json',
                [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offer']]
            ),
            JSON_FORCE_OBJECT,
            512,
            JSON_THROW_ON_ERROR
        );

        return $this->createApiResponse(['suggestions' => $suggestions]);
    }

    /**
     * @param string $likeSql
     * @param int $countSuggestions
     * @return array
     */
    private function getSuggestions(string $likeSql, int $countSuggestions): array
    {
        $manager = $this->getDoctrine()->getManager();

        /** @var OfferRepository $repository */
        $repository = $manager->getRepository(Offer::class);

        return $repository->getSearchSuggestions($likeSql, $countSuggestions);
    }
}
