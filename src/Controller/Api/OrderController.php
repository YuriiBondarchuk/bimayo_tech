<?php

namespace App\Controller\Api;

use Exception;
use JsonException;
use App\Entity\site\Order;
use App\Service\Api\ApiResponse;
use App\Service\Site\Manager\OrderManager;
use App\Service\Site\Manager\MailerManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class OrderController extends ApiControllerBase
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|JsonException
     */
    #[Route('/api/checkout/order', name: 'app_api_checkout_order', methods: ['POST'])]
    public function order(Request $request, LoggerInterface $logger): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if ($data === null || !array_key_exists('orderData', $data)) {
            throw new BadRequestHttpException('Invalid JSON');
        }

        try {
            $items = $data['orderData']['items'] ?? null;

            if (empty($items)) {
                $message = nl2br(
                    "Ваш список замовлень порожній! Спочатку додайте товар до кошику! \n
                    Якщо помилка повторюється повідомте нам на сторінці <a href='/contact'>Контакти</a> і ми обов'язково Вам допоможемо"
                );

                throw new BadRequestHttpException('Order items is empty');
            }

            $order = new OrderManager($this->getDoctrine());
            $newOrder = $order->createOrder($data['orderData']);
            $message = sprintf(
                "Ваше замовлення створенно успішнно! Номер Вашого замовлення %s.\n Ми зв'яжемось з Вами найближчим часом!",
                $newOrder->getId()
            );

            $data['orderData']['order'] = $newOrder;
            $deliveryType = array_search($newOrder->getDeliveryType(), Order::getDeliveryTypeChoices());
            $paymentType = array_search($newOrder->getPaymentType(), Order::getPaymentTypeChoices());

            $data['orderData']['paymentType'] = $paymentType;
            $data['orderData']['deliveryType'] = $deliveryType;

            $successfully = true;
            $mail = new MailerManager($this->getParameter('app.mailer_dsn'), $logger);
            $mail->sendMail($data['orderData'], true);
        } catch (Exception $e) {
            if (empty($message)) {
                $message = nl2br(
                    "Під час створення замовлення виникла помилка! Спробуйте будь-ласка пізніше.\n
             Якщо помилка повторюється повідомте нам на сторінці <a href='/contact'>Контакти</a> і ми обов'язково Вам допоможемо"
                );
            }

            $successfully = false;

            $this->getSiteLogger()->error($e->getMessage());
        }

        return $this->createApiResponse([
            'message' => $message,
            'successfully' => $successfully
        ], ApiResponse::STATUS_SUCCESS);
    }
}
