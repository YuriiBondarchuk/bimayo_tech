<?php

namespace App\Controller\Api;

use App\Entity\site\Offer;
use App\Repository\site\OfferRepository;
use JsonException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class RecommendedItemsController extends ApiControllerBase
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|JsonException
     */
    #[Route('/api/recommendedItems', name: 'app_api_recommended_items')]
    public function features(SerializerInterface $serializer, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if ($data === null || !array_key_exists('category', $data)) {
            throw new BadRequestHttpException('Invalid JSON');
        }

        $category = $data['category'] ?? false;

        $recommendedItems = json_decode(
            $serializer->serialize(
                $this->getRecommendedItems($category), 'json',
                [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offer']]
            ),
            JSON_OBJECT_AS_ARRAY,
            512,
            JSON_THROW_ON_ERROR
        );

        return $this->createApiResponse([
            'items' => $recommendedItems
        ]);
    }

    /**
     * @param int|bool $category
     * @return mixed
     */
    private function getRecommendedItems(int|bool $categoryImportId): mixed
    {
        $manager = $this->getDoctrine()->getManager();

        /** @var OfferRepository $repository */
        $repository = $manager->getRepository(Offer::class);

        if ($categoryImportId) {
            return $repository->getRandomOffersByCategory($categoryImportId);
        }

        return $repository->getRecommendedItems();
    }
}
