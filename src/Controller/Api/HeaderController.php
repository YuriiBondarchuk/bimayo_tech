<?php

namespace App\Controller\Api;

use App\Entity\site\Info;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HeaderController extends ApiControllerBase
{
    private array $headerMenu = [
        [
            'name' => "Головна",
            'route' => '/',
        ],
        [
            'name' => "Товари",
            'route' => '/products',
        ],
        [
            'name' => "Контакти",
            'route' => '/contact',
        ],
        [
            'name' => "Про нас",
            'route' => '/about_us',
        ],
    ];

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/api/header', name: 'app_api_header')]
    public function info(Request $requestStack): JsonResponse
    {
        $infoRepository = $this->getDoctrine()->getRepository(Info::class);
        $info = $infoRepository->findAll();
        $info = reset($info);

        $route = $requestStack->headers->get('referer');
        $routeName = substr($route, strrpos($route, $requestStack->getHost()) + strlen($requestStack->getHost()));

        $this->prepareHeaderMenu($routeName);

        return $this->createApiResponse([
            'info' => $info,
            'headerMenu' => $this->headerMenu
        ]);
    }

    /**
     * @param string $routeName
     * @return void
     */
    private function prepareHeaderMenu(string $routeName): void
    {
        array_walk($this->headerMenu, function ($menuItem, $key) use ($routeName) {
            $this->headerMenu[$key]['active'] = true;
            if ($menuItem['route'] !== $routeName) {
                $this->headerMenu[$key]['active'] = false;
            }
        });
    }
}
