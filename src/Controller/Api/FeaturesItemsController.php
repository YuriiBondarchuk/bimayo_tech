<?php

namespace App\Controller\Api;

use App\Entity\site\Offer;
use App\Repository\site\OfferRepository;
use App\Service\Admin\Manager\CategoryManager;
use App\Service\Admin\Translator\Translator;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use ErrorException;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Knp\Component\Pager\Pagination\PaginationInterface as SlidingPaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class FeaturesItemsController extends ApiControllerBase
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|JsonException
     * @throws ExceptionInterface
     * @throws ErrorException
     */
    #[Route('/api/featureItems', name: 'app_api_feature_items', methods: ['POST'])]
    public function features(Request $request, PaginatorInterface $paginator): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if ($data === null || !array_key_exists('randomOffers', $data)) {
            throw new BadRequestHttpException('Invalid JSON');
        }

        $randomOffers = $data['randomOffers'];
        $pageNumber = $data['pageNumber'] ?? false;
        $categoryUrl = false;
        $filters = $data['filters'] ?? false;

        if (isset($data['categoryUrl']) && $data['categoryUrl'] !== 'products') {
            $categoryUrl = $data['categoryUrl'];
        }


        $paginatorData = [];
        $priceRange = [];

        if ($randomOffers && !$pageNumber) {
            $offers = $this->getRandomOffers();
        } else {
            $offersQueryBuilder = $this->getOffersByIds($categoryUrl);

            if ($filters) {
                $this->applyFilters($offersQueryBuilder, $filters);
            }

            $priceRange = $this->getMinMaxPrice($categoryUrl);

            $paginator = $paginator->paginate($offersQueryBuilder, $pageNumber, 9);

            $offers = $paginator->getItems();

            $paginatorData = $this->getDataForPaginator($paginator);
        }

        $this->translateOffers($offers);

        $featuresItems = json_decode(
            $this->getSerializer()->serialize(
                $offers, 'json',
                [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offer']]
            ),
            JSON_FORCE_OBJECT,
            512,
            JSON_THROW_ON_ERROR
        );

        return $this->createApiResponse([
            'items' => $featuresItems,
            'paginatorData' => $paginatorData,
            'priceRange' => $priceRange
        ]);
    }

    /**
     * @return mixed
     */
    private function getRandomOffers(): mixed
    {
        $offerRepository = $this->getOfferRepository();

        $categoryId = $offerRepository->getRandomCategoryId();
        return $offerRepository->getRandomOffersByCategory($categoryId);
    }

    /**
     * @param int|null $categoryUrl
     * @return mixed
     * @throws ExceptionInterface
     */
    private function getOffersByIds(?string $categoryUrl): mixed
    {
        $categoryIds = $this->getCategoryImportIdsByUrl($categoryUrl);

        $repository = $this->getOfferRepository();

        return $repository->getOffersByIds($categoryIds);
    }

    #[ArrayShape([
        'activePage' => "int",
        'pageRangeDisplayed' => "int",
        'itemsCountPerPage' => "int",
        'totalItemsCount' => "int"
    ])]
    private function getDataForPaginator(SlidingPaginationInterface $paginator): array
    {
        return [
            'activePage' => $paginator->getCurrentPageNumber(),
            'pageRangeDisplayed' => 5,
            'itemsCountPerPage' => $paginator->getItemNumberPerPage(),
            'totalItemsCount' => $paginator->getTotalItemCount()
        ];
    }

    /**
     * @param array $offers
     * @return void
     * @throws ErrorException
     */
    private function translateOffers(array &$offers): void
    {
        $translator = new Translator();
        foreach ($offers as $key => $offer) {
            if ($offer->getNeedTranslate()) {
                $offer->setName($translator->getTranslator()->translate($offer->getName()));
                $offers[$key] = $offer;
            }
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $filters
     * @return void
     */
    private function applyFilters(QueryBuilder $queryBuilder, array $filters): void
    {
        if (isset($filters['priceRange'])) {
            $priceMin = $filters['priceRange']['min'];
            $priceMax = $filters['priceRange']['max'];

            $queryBuilder
                ->andWhere('o.price BETWEEN :min AND :max')
                ->setParameter('min', $priceMin)
                ->setParameter('max', $priceMax);
        }
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    private function getMinMaxPrice(?string $categoryUrl): array
    {
        $categoryIds = $this->getCategoryImportIdsByUrl($categoryUrl);

        $repository = $this->getOfferRepository();

        return $repository->getPriceRangeByCategoryIds($categoryIds);
    }

    /**
     * @param string|null $categoryUrl
     * @return array
     */
    private function getCategoryImportIdsByUrl(?string $categoryUrl): array
    {
        $categoryManager = new CategoryManager($this->getDoctrine(), $this->getSerializer());

        if ($categoryUrl) {
            $categoryIds = $categoryManager->getCategoryImportIdsByUrl($categoryUrl);
        } else {
            $categoryIds = $categoryManager->getAllCategoryIds();
        }

        return $categoryIds;
    }

    /**
     * @return OfferRepository
     */
    private function getOfferRepository(): OfferRepository
    {
        $manager = $this->getDoctrine()->getManager();

        /** @var OfferRepository $repository */
        $repository = $manager->getRepository(Offer::class);

        return $repository;
    }
}
