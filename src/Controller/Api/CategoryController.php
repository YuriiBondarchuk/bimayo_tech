<?php

namespace App\Controller\Api;

use App\Service\Admin\Manager\CategoryManager;
use App\Utils\CacheHelper;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends ApiControllerBase
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/api/category', name: 'app_api_category')]
    public function category(): JsonResponse
    {
        $categoryManager = new CategoryManager($this->getDoctrine(), $this->getSerializer());

        $cache = new FilesystemAdapter('', 0, $this->getParameter('kernel.cache_dir'));

        $categoryTree = CacheHelper::getOrSet($cache, 'category.tree', static function () use ($categoryManager) {
            return $categoryManager->buildCategoryTree();
        });

        return $this->createApiResponse([
            'category' => $categoryTree
        ]);
    }
}
