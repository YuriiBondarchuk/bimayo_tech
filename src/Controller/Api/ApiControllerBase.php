<?php

namespace App\Controller\Api;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class ApiControllerBase extends AbstractController
{
    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @var FilesystemAdapter $cache
     */
    private FilesystemAdapter $cache;

    /**
     * @param ManagerRegistry $doctrine
     * @param SerializerInterface $serializer
     * @param LoggerInterface $siteLogger
     */
    public function __construct(ManagerRegistry $doctrine, SerializerInterface $serializer, LoggerInterface $siteLogger)
    {
        $this->setCache();
        $this->setDoctrine($doctrine);
        $this->setSerializer($serializer);
        $this->setSiteLogger($siteLogger);
    }

    /**
     * @param mixed $data Usually an object you want to serialize
     * @param int $statusCode
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function createApiResponse(mixed $data, int $statusCode = 200): object
    {
        $json = $this->container->get('serializer')->serialize($data, 'json');

        return new JsonResponse($json, $statusCode, [], true);
    }

    /**
     * @return ManagerRegistry
     */
    protected function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    private function setDoctrine(ManagerRegistry $doctrine): void
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @return LoggerInterface
     */
    protected function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }

    /**
     * @param LoggerInterface $siteLogger
     * @return void
     */
    protected function setSiteLogger(LoggerInterface $siteLogger): void
    {
        $this->siteLogger = $siteLogger;
    }

    /**
     * @return FilesystemAdapter
     */
    public function getCache(): FilesystemAdapter
    {
        return $this->cache;
    }

    /**
     * @return void
     */
    public function setCache(): void
    {
        $this->cache = new FilesystemAdapter();
    }
}