<?php

namespace App\Controller\Api;

use App\Service\Api\ApiResponse;
use App\Service\Site\Manager\MailerManager;
use Exception;
use JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class QuestionFormController extends ApiControllerBase
{
    /**
     * @throws JsonException
     */
    #[Route('/api/question', name: 'api_question', methods: ["POST"])]
    public function index(Request $request, KernelInterface $appKernel): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $mail = new MailerManager($this->getParameter('app.mailer_dsn'), $this->getSiteLogger());

        try {
            $mail->sendMail($data['formData'], false);

            $message = "Ваше повідомлення відправлено успішнно!\n Ми зв'яжемось з Вами найближчим часом!";

            $successfully = true;
        } catch (Exception $e) {
            $message = nl2br(
                "Під час відправлення повідомлення виникла помилка! Спробуйте будь-ласка пізніше.\n
             Якщо помилка повторюється повідомте нам на сторінці <a href='/contact'>Контакти</a> і ми обов'язково Вам допоможемо"
            );

            $successfully = false;

            $this->getSiteLogger()->error($e->getMessage());
        }

        return $this->createApiResponse([
            'message' => $message,
            'successfully' => $successfully
        ], ApiResponse::STATUS_SUCCESS);
    }
}
