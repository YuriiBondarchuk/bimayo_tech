<?php

namespace App\Controller;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Repository\site\CategoryRepository;
use App\Repository\site\OfferRepository;
use App\Service\Admin\Translator\Translator;
use App\Service\Breadcrumbs\Breadcrumb;
use App\Service\Breadcrumbs\Breadcrumbs;
use Exception;
use JsonException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ProductController extends AbstractControllerBase implements HasBreadcrumbsInterface
{
    use HasBreadcrumbsTrait;

    private array $parentCategories = [];

    private array $currentOffer;

    /**
     * @throws JsonException
     * @throws \ErrorException
     */
    #[Route('/product/{url}', name: 'app_product', methods: ["GET"])]
    public function index(Offer $offer, SerializerInterface $serializer): Response
    {
        $this->currentOffer = json_decode(
            $serializer->serialize(
                $offer, 'json',
                [AbstractNormalizer::IGNORED_ATTRIBUTES => ['offer']]
            ),
            JSON_OBJECT_AS_ARRAY,
            512,
            JSON_THROW_ON_ERROR
        );

        $this->currentOffer['params'] = json_decode(
            $this->currentOffer['offerParam']['params'],
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        unset ($this->currentOffer['offerParam']);

        $this->currentOffer['pictures'] = json_decode(
            $this->currentOffer['offerPicture']['pictures'],
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        if ($this->currentOffer['needTranslate']) {
            $translator = new Translator();
            $this->currentOffer['name'] = $translator->getTranslator()->translate($this->currentOffer['name']);
            $this->currentOffer['description'] = $translator->getTranslator()->translate(
                $this->currentOffer['description']
            );
        }

        try {
            $this->prepareIdenticalOffers($this->currentOffer);
        } catch (Exception $e) {
            $this->getSiteLogger()->error($e->getMessage());
            throw new \ErrorException();
        }

        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $category = $categoryRepository->getCategoryByOffer($this->currentOffer);

        $this->buildCategoriesTreeByCategoryId($category, $categoryRepository);

        return $this->render('pages/product.html.twig', [
            'productData' => $this->currentOffer,
            'pageName' => $category->getName(),
            'url' => $this->getCurrentUri(),
            'breadcrumbs' => $this->configureBreadcrumbs()
        ]);
    }

    /**
     * @param array $offer
     * @throws JsonException
     */
    private function prepareIdenticalOffers(array &$offer): void
    {
        /** @var OfferRepository $offerRepository */
        $offerRepository = $this->getDoctrine()->getRepository(Offer::class);
        $identicalOffers = $offerRepository->findIdenticalOffers($offer);

        $color = [];
        $size = [];
        if (isset($offer['params']['Колір'])) {
            $color[] = $offer['params']['Колір'];
        }
        if (isset($offer['params']['Розмір'])) {
            $size[] = $offer['params']['Розмір'];
        }

        foreach ($identicalOffers as $identicalOffer) {
            /**@var Offer $identicalOffer */
            $params = json_decode($identicalOffer->getOfferParam()->getParams(), true, 512, JSON_THROW_ON_ERROR);

            if (isset($params['Колір']) && !in_array($params['Колір'], $color, true)) {
                $color[] = $params['Колір'];
            }

            if (isset($params['Розмір']) && !in_array($params['Розмір'], $size, true)) {
                $size[] = $params['Розмір'];
            }
        }

        $offer['existColor'] = $color;
        $offer['existSize'] = $size;
    }

    /**
     * @return array
     */
    public function configureBreadcrumbs(): array
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs
            ->add(new Breadcrumb('🏠', '/'));

        $parentCategories = array_reverse($this->parentCategories);

        /** @var Category $parentCategory */
        foreach ($parentCategories as $parentCategory) {
            $breadcrumbs->add(new Breadcrumb($parentCategory->getName(), '/products/' . $parentCategory->getUrl()));
        }

        $breadcrumbs->add(
            new Breadcrumb($this->currentOffer['name'], $this->getCurrentUri())
        );

        return $this->convertBreadcrumbsToArray($breadcrumbs);
    }
}
