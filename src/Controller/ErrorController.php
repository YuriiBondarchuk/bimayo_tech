<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{
//    #[Route(name: 'app_error')]
    public function show(): Response
    {
        return $this->render('pages/error.html.twig');
    }
}