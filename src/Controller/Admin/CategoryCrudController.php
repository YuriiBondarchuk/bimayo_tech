<?php

namespace App\Controller\Admin;

use App\Entity\site\Category;
use App\Utils\CacheHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function delete(AdminContext $context): RedirectResponse
    {
        $response = parent::delete($context);
        $cache = new FilesystemAdapter('', 0, $this->getParameter('kernel.cache_dir'));

        CacheHelper::delete($cache, 'category.tree');

        return $response;
    }

    public function configureActions(Actions $actions): Actions
    {
        $clearCache = Action::new('clearCategoryCache', 'Clear Cache CategoryTree', 'fa fa-trash ')
            ->createAsGlobalAction('')
            ->linkToCrudAction('clearCategoryCache')
            ->addCssClass('btn btn-success');

        return parent::configureActions($actions)
            ->add(Crud::PAGE_INDEX, $clearCache);
    }

    public function clearCategoryCache(Request $request): RedirectResponse
    {
        $cache = new FilesystemAdapter('', 0, $this->getParameter('kernel.cache_dir'));
        CacheHelper::delete($cache, 'category.tree');

        return new RedirectResponse($request->get('referrer'));
    }
}
