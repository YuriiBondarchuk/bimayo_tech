<?php

namespace App\Controller\Admin;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Entity\site\Vendor;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class VendorCrudController extends AbstractCrudControllerBase
{
    public static function getEntityFqcn(): string
    {
        return Vendor::class;
    }

    /**
     * @throws \ErrorException
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $uow = $entityManager->getUnitOfWork();
        $uow->computeChangeSets();

        $changeset = $uow->getEntityChangeSet($entityInstance);

        if ($changeset['margin'] ?? false) {
            try {
                $this->updateOfferPriceMargin($entityInstance, $changeset['margin'][1]);
            } catch (Exception $e) {
                $this->getSiteLogger()->error($e->getMessage());
                throw new \ErrorException();
            }
        }

        parent::updateEntity($entityManager, $entityInstance);
    }

    private function updateOfferPriceMargin(Vendor $vendor, int $newMargin): void
    {
        $offerManager = $this->getDoctrine()->getManagerForClass(Offer::class);
        $offerRepository = $offerManager->getRepository(Offer::class);
        $offers = $offerRepository->findBy(['vendor' => $vendor]);

        $batchSize = 500;
        $i = 1;
        /** @var Offer $offer */
        foreach ($offers as $offer) {
            if ($newMargin === 0) {
                $priceWithMargin = $offer->getPrice();
            } else {
                $result = $offer->getPrice() / 100 * $newMargin;
                $priceMargin = $offer->getPrice() + $result;
                $priceWithMargin = round($priceMargin);
            }

            $offer->setPriceMargin($priceWithMargin);

            if (($i % $batchSize) === 0) {
                $offerManager->flush();
            }

            ++$i;
        }

        $this->getSiteLogger()->debug(round(memory_get_usage() / 1024 / 1024, 2) . ' MB' . PHP_EOL);

        $offerManager->flush();
    }
}
