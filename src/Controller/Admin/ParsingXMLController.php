<?php

namespace App\Controller\Admin;

use App\Form\Parser\ParserFormType;
use App\Service\Admin\Parser\ParserXML;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParsingXMLController extends DashboardController
{
    #[Route('admin/parsing_index', name: 'app_parsing_index')]
    public function index(): Response
    {
        $parserForm = $this->createForm(ParserFormType::class);

        return $this->render('admin/parsing_xml/index.html.twig', [
            'controller_name' => 'ParsingXMLController',
            'parserForm' => $parserForm->createView(),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('admin/parsing_import', name: 'app_parsing_import')]
    public function import(Request $request, ManagerRegistry $managerRegistry): Response
    {
        $form = $this->createForm(ParserFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['file']->getData();
            $needTranslate = $form['translate']->getData();

            if (!$uploadedFile) {
                $this->addFlash('warning', 'Please select the file!');

                return $this->redirectToRoute('app_parsing_index');
            }

            if ($uploadedFile->guessExtension() !== ParserXML::REQUIRED_EXTENSION) {
                $this->addFlash(
                    'danger',
                    sprintf(
                        'The file has an incorrect extension. Please upload the file only with \'%s\' extension',
                        ParserXML::REQUIRED_EXTENSION
                    )
                );

                return $this->redirectToRoute('app_parsing_index');
            }

            return $this->parseXML($uploadedFile, $needTranslate, $managerRegistry);
        }

        $this->addFlash('warning', 'Form not submitted or has incorrect data');
        return $this->redirectToRoute('app_parsing_index');
    }

    /**
     * @param UploadedFile $file
     * @param bool $needTranslate
     * @param ManagerRegistry $managerRegistry
     * @return RedirectResponse
     * @throws Exception
     */
    private function parseXML(
        UploadedFile $file,
        bool $needTranslate,
        ManagerRegistry $managerRegistry
    ): RedirectResponse {
        $parserXML = new ParserXML($file, $managerRegistry, $this->getSiteLogger());

        try {
            $parserXML->parse($needTranslate);
        } catch (Exception $e) {
            $this->getSiteLogger()->error($e->getMessage());

            throw new Exception();
        }

        $this->addFlash('success', 'File successfully imported to DB');

        return $this->redirectToRoute('app_parsing_index');
    }
}

