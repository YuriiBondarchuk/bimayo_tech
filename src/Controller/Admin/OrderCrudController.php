<?php

namespace App\Controller\Admin;

use App\Entity\site\Offer;
use App\Entity\site\Order;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInPlural('Orders')
            ->setEntityLabelInSingular('Order');
    }

//    public function configureFilters(Filters $filters): Filters
//    {
//        return parent::configureFilters($filters)->add('category');
//    }
}
