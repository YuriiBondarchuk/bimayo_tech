<?php

namespace App\Controller\Admin;

use App\Entity\site\Category;
use App\Entity\site\Offer;
use App\Entity\site\Order;
use App\Entity\site\Vendor;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @param LoggerInterface $siteLogger
     **/
    public function __construct(LoggerInterface $siteLogger)
    {
        $this->siteLogger = $siteLogger;
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Bimayo');
    }

    /**
     * @return iterable
     */
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Control panel', 'fa fa-home');

        yield MenuItem::section('import files');
        yield MenuItem::linkToRoute('Import to DB', "fas fa-paste", 'app_parsing_index');

        yield MenuItem::section('Site schema setting');
        yield MenuItem::linkToCrud('Offers', 'fa-brands fa-buffer', Offer::class);
        yield MenuItem::linkToCrud('Vendors', 'fa fa-industry', Vendor::class);
        yield MenuItem::linkToCrud('Orders', 'fa-sharp fa-solid fa-file-invoice-dollar', Order::class);
        yield MenuItem::linkToCrud('Categories', 'fa fa-list-alt', Category::class);

        yield MenuItem::section('Admin schema setting');
    }

    /**
     * @return LoggerInterface
     */
    protected function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }
}
