<?php

namespace App\Controller\Admin;

use App\Entity\site\Offer;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class OfferCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Offer::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInPlural('Offers')
            ->setEntityLabelInSingular('Offer')
            ->setPaginatorPageSize(20);
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('vendor');
        yield AssociationField::new('category');
        yield TextField::new('currency_name')->setDisabled();
        yield TextField::new('url');
        yield NumberField::new('price');
        yield NumberField::new('priceln');
        yield TextField::new('vendor_code');
        yield TextField::new('name');
        yield TextareaField::new('description');
        yield TextField::new('barcode');
        yield TextareaField::new('keywords');
        yield BooleanField::new('is_recommended');
        yield BooleanField::new('need_translate');
        yield DateTimeField::new('t_ins');
        yield NumberField::new('old_price');
        yield BooleanField::new('available');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return parent::configureFilters($filters)
            ->add('vendor');
    }
}
