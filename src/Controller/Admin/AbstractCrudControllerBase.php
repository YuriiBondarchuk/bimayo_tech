<?php

namespace App\Controller\Admin;

use Doctrine\Persistence\ManagerRegistry;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Psr\Log\LoggerInterface;

abstract class AbstractCrudControllerBase extends AbstractCrudController
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $siteLogger;

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @param LoggerInterface $siteLogger
     * @param ManagerRegistry $doctrine
     */
    public function __construct(LoggerInterface $siteLogger, ManagerRegistry $doctrine,)
    {
        $this->setSiteLogger($siteLogger);
        $this->setDoctrine($doctrine);
    }

    /**
     * @return LoggerInterface
     */
    protected function getSiteLogger(): LoggerInterface
    {
        return $this->siteLogger;
    }

    /**
     * @param LoggerInterface $siteLogger
     */
    protected function setSiteLogger(LoggerInterface $siteLogger): void
    {
        $this->siteLogger = $siteLogger;
    }

    /**
     * @return ManagerRegistry
     */
    protected function getDoctrine(): ManagerRegistry
    {
        return $this->doctrine;
    }

    /**
     * @param ManagerRegistry $doctrine
     */
    private function setDoctrine(ManagerRegistry $doctrine): void
    {
        $this->doctrine = $doctrine;
    }
}