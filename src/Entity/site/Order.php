<?php

namespace App\Entity\site;


use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\site\OrderRepository;

/**
 * Order
 */
#[ORM\Table(name: "site.order")]
#[ORM\UniqueConstraint(name: "order_offer_id_key", columns: ["offer_id"])]
#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[Assert\EnableAutoMapping]
class Order
{
    //delivery type
    public const NOVA_POSHTA = 1;
    public const UKR_POSHTA = 2;

    //payment type
    public const PAID_ON_CARD = 1;
    public const POST_PAID = 2;

    //status
    public const STATUS_NEW = 1;
    public const STATUS_IN_PROCESS = 2;
    public const STATUS_DONE = 3;
    public const STATUS_CANCEL = 4;

    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.order_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "user_name", type: "string", nullable: false)]
    private string $userName;

    #[ORM\Column(name: "user_surname", type: "string", nullable: false)]
    private string $userSurname;

    #[ORM\Column(name: "user_phone", type: "string", nullable: false)]
    private string $userPhone;

    #[ORM\Column(name: "user_email", type: "string", nullable: false)]
    private string $userEmail;

    #[ORM\Column(name: "status", type: "integer", nullable: false)]
    private int $status;

    #[ORM\Column(name: "payment_type", type: "integer", nullable: false)]
    private int $paymentType;

    #[ORM\Column(name: "delivery_type", type: "integer", nullable: false)]
    private int $deliveryType;

    #[ORM\Column(name: "post_city", type: "text", nullable: false)]
    private string $postCity;

    #[ORM\Column(name: "post_office", type: "text", nullable: false)]
    private string $postOffice;

    #[ORM\Column(name: "order_total_price", type: "float", precision: 10, scale: 2, options: ["default" => 0])]
    private float $orderTotalPrice = 0;

    #[ORM\Column(name: "comment", type: "text", nullable: true)]
    private string $comment;

    #[ORM\Column(name: "t_ins", type: "datetime", nullable: false, options: ["default" => "CURRENT_TIMESTAMP(0)"])]
    private string|\DateTimeInterface $t_ins;

    #[ORM\Column(name: "t_upd", type: "datetime", nullable: true)]
    private \DateTimeInterface $t_upd;

    #[ORM\OneToMany(mappedBy: "order", targetEntity: OrderOffer::class, fetch: "LAZY")]
    private ArrayCollection|Collection $orderOffer;

    public function __construct()
    {
        $this->orderOffer = new ArrayCollection();
    }

    /**
     * @param $orderOffer
     * @return $this
     */
    public function addOrderOffer($orderOffer): self
    {
        if (!$this->orderOffer->contains($orderOffer)) {
            $this->orderOffer[] = $orderOffer;
            $orderOffer->setOrder($this);
        }

        return $this;
    }

    /**
     * @param $orderOffer
     * @return $this
     */
    public function removeOrderOffer($orderOffer): self
    {
        if ($this->orderOffer->contains($orderOffer)) {
            $this->orderOffer->removeElement($orderOffer);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getPaymentType(): int
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType(int $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return int
     */
    public function getDeliveryType(): int
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType(int $deliveryType): void
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return string
     */
    public function getPostCity(): string
    {
        return $this->postCity;
    }

    /**
     * @param string $postCity
     */
    public function setPostCity(string $postCity): void
    {
        $this->postCity = $postCity;
    }

    /**
     * @return string
     */
    public function getPostOffice(): string
    {
        return $this->postOffice;
    }

    /**
     * @param string $postOffice
     */
    public function setPostOffice(string $postOffice): void
    {
        $this->postOffice = $postOffice;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return DateTimeInterface|string
     */
    public function getTIns(): DateTimeInterface|string
    {
        return $this->t_ins;
    }

    /**
     * @param DateTimeInterface|string $t_ins
     */
    public function setTIns(DateTimeInterface|string $t_ins): void
    {
        $this->t_ins = $t_ins;
    }

    /**
     * @return DateTimeInterface
     */
    public function getTUpd(): DateTimeInterface
    {
        return $this->t_upd;
    }

    /**
     * @param DateTimeInterface $t_upd
     */
    public function setTUpd(DateTimeInterface $t_upd): void
    {
        $this->t_upd = $t_upd;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getUserSurname(): string
    {
        return $this->userSurname;
    }

    /**
     * @param string $userSurname
     */
    public function setUserSurname(string $userSurname): void
    {
        $this->userSurname = $userSurname;
    }

    /**
     * @return string
     */
    public function getUserPhone(): string
    {
        return $this->userPhone;
    }

    /**
     * @param string $userPhone
     */
    public function setUserPhone(string $userPhone): void
    {
        $this->userPhone = $userPhone;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @param string $userEmail
     */
    public function setUserEmail(string $userEmail): void
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getOrderTotalPrice(): string
    {
        return $this->orderTotalPrice;
    }

    /**
     * @param string $orderTotalPrice
     */
    public function setOrderTotalPrice(string $orderTotalPrice): void
    {
        $this->orderTotalPrice = $orderTotalPrice;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->setTIns(new DateTime());
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->setTUpd(new DateTime());
    }

    /**
     * @return array
     */
    #[ArrayShape(["Нова Пошта" => "int", "Укрпошта" => "int"])]
    public static function getDeliveryTypeChoices(): array
    {
        return [
            "Нова Пошта" => self::NOVA_POSHTA,
            "Укрпошта" => self::UKR_POSHTA
        ];
    }

    /**
     * @return array
     */
    #[ArrayShape([
        "Оплата на банківську картку (Приват, Моно)" => "int",
        "Оплата після отримання посилки у відділенні" => "int"
    ])]
    public static function getPaymentTypeChoices(): array
    {
        return [
            "Оплата на банківську картку (Приват, Моно)" => self::PAID_ON_CARD,
            "Оплата після отримання посилки у відділенні" => self::POST_PAID
        ];
    }
}
