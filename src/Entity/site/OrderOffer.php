<?php

namespace App\Entity\site;

use App\Repository\site\OrderOfferRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * OrderOffer
 */
#[ORM\Table(name: "site.order_offer")]
#[ORM\UniqueConstraint(name: "order_offer_order_id_key", columns: ["order_id"])]
#[ORM\Entity(repositoryClass: OrderOfferRepository::class)]
class OrderOffer
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.order_offer_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "offer_size", type: "string", length: 180, nullable: true)]
    private string $offerSize;

    #[ORM\Column(name: "offer_color", type: "string", length: 180, nullable: true)]
    private string $offerColor;

    #[ORM\Column(name: "quantity", type: "smallint", nullable: false)]
    private int $quantity;

    #[ORM\Column(name: "offer_price", type: "float", precision: 10, scale: 2, nullable: false)]
    private float $offerPrice;

    #[ORM\Column(name: "total_price", type: "float", precision: 10, scale: 2, nullable: false)]
    private float $totalPrice;

    #[ORM\ManyToOne(targetEntity: Offer::class, fetch: 'LAZY', inversedBy: "order")]
    private Offer $offer;

    #[ORM\ManyToOne(targetEntity: Order::class, fetch: 'LAZY', inversedBy: "order")]
    private Order $order;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOfferSize(): string
    {
        return $this->offerSize;
    }

    /**
     * @param string $offerSize
     */
    public function setOfferSize(string $offerSize): void
    {
        $this->offerSize = $offerSize;
    }

    /**
     * @return string
     */
    public function getOfferColor(): string
    {
        return $this->offerColor;
    }

    /**
     * @param string $offerColor
     */
    public function setOfferColor(string $offerColor): void
    {
        $this->offerColor = $offerColor;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getOfferPrice(): float
    {
        return $this->offerPrice;
    }

    /**
     * @param float $offerPrice
     */
    public function setOfferPrice(float $offerPrice): void
    {
        $this->offerPrice = $offerPrice;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice(float $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     */
    public function setOffer(Offer $offer): void
    {
        $this->offer = $offer;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }
}
