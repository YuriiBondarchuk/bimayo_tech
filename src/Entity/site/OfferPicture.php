<?php

namespace App\Entity\site;

use App\Repository\site\OfferPictureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * OfferPicture
 */
#[ORM\Table(name: "site.offer_picture")]
#[ORM\UniqueConstraint(name: "offer_picture_offer_id_key", columns: ["offer_id"])]
#[ORM\Entity(repositoryClass: OfferPictureRepository::class)]
class OfferPicture
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.offer_picture_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "pictures", type: "json", nullable: true)]
    private ?string $pictures;

    #[ORM\OneToOne(inversedBy: "offerPicture", targetEntity: Offer::class, cascade: ["persist"])]
    private Offer $offer;

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     */
    public function setOffer(Offer $offer): void
    {
        $this->offer = $offer;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPictures(): ?string
    {
        return $this->pictures;
    }

    /**
     * @param string|null $pictures
     * @return $this
     */
    public function setPictures(?string $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }
}
