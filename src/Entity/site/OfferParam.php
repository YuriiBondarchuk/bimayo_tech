<?php

namespace App\Entity\site;

use App\Repository\site\OfferParamRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * OfferParam
 */
#[ORM\Table(name: "site.offer_param")]
#[ORM\UniqueConstraint(name: "offer_param_offer_id_key", columns: ["offer_id"])]
#[ORM\Entity(repositoryClass: OfferParamRepository::class)]
class OfferParam
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.offer_param_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "params", type: "json", nullable: true)]
    private ?string $params;

    #[ORM\OneToOne(inversedBy: "offerParam", targetEntity: Offer::class, cascade: ["persist"])]
    private Offer $offer;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     */
    public function setOffer(Offer $offer): void
    {
        $this->offer = $offer;
    }

    /**
     * @return string|null
     */
    public function getParams(): ?string
    {
        return $this->params;
    }

    /**
     * @param string|null $params
     * @return $this
     */
    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }
}
