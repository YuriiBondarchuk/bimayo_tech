<?php

namespace App\Entity\site;

use App\Repository\site\OfferRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
/**
 * Offer
 */
#[ORM\Table(name: "site.offer")]
#[ORM\UniqueConstraint(name: "offer_category_id_key", columns: ["category_id"])]
#[ORM\Entity(repositoryClass: OfferRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Offer
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.offer_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Category::class, fetch: 'LAZY', inversedBy: 'offers')]
    private Category $category;

    #[ORM\Column(name: "currency_name", type: "string", nullable: false)]
    private string $currencyName;

    #[ORM\Column(name: "available", type: "boolean", nullable: true)]
    private ?bool $available;

    #[ORM\Column(name: "selling_type", type: "string", length: 255, nullable: true)]
    private ?string $sellingType;

    #[ORM\Column(name: "url", type: "string", length: 255, nullable: true)]
    private ?string $url;

    #[ORM\Column(name: "price", type: "float", precision: 10, scale: 0, nullable: false)]
    private float $price;

    #[ORM\Column(name: "price_margin", type: "float", precision: 10, scale: 0, nullable: false)]
    private float $priceMargin;

    #[ORM\Column(name: "vendor_code", type: "string", length: 255, nullable: true)]
    private ?string $vendorCode;

    #[ORM\Column(name: "delivery", type: "boolean", nullable: true)]
    private ?bool $delivery;

    #[ORM\Column(name: "name", type: "string", length: 255, nullable: false)]
    private string $name;

    #[ORM\Column(name: "description", type: "text", nullable: true)]
    private ?string $description;

    #[ORM\Column(name: "barcode", type: "string", nullable: true)]
    private ?string $barcode;

    #[ORM\Column(name: "portal_category_id", type: "integer", nullable: true)]
    private ?int $portalCategoryId;
    #[ORM\ManyToOne(targetEntity: Vendor::class, cascade: ["persist"], fetch: 'LAZY', inversedBy: 'offers')]
    private Vendor $vendor;

    #[ORM\Column(name: "keywords", type: "text", nullable: true)]
    private ?string $keywords;

    #[ORM\Column(name: "is_recommended", type: "boolean", nullable: true)]
    private ?bool $isRecommended;

    #[ORM\Column(name: "need_translate", type: "boolean", nullable: false)]
    private bool $needTranslate;

    #[ORM\OneToOne(mappedBy: "offer", targetEntity: OfferParam::class)]
    private OfferParam $offerParam;

    #[ORM\OneToOne(mappedBy: "offer", targetEntity: OfferPicture::class)]
    private OfferPicture $offerPicture;

    #[ORM\Column(name: "t_ins", type: "datetime", nullable: false, options: ["default" => "CURRENT_TIMESTAMP(0)"])]
    private string|DateTimeInterface $t_ins;

    #[ORM\Column(name: "old_price", type: "float", nullable: true)]
    private ?float $oldPrice;

    #[ORM\Column(name: "category_import_id", type: "integer", nullable: true)]
    private ?int $categoryImportId;

    public function __isset($prop) : bool
    {
        return isset($this->$prop);
    }

        /**
     * @return OfferPicture
     */
    public function getOfferPicture(): OfferPicture
    {
        return $this->offerPicture;
    }

    /**
     * @param mixed $offerPicture
     */
    public function setPictures(mixed $offerPicture): void
    {
        $this->offerPicture = $offerPicture;
    }

    /**
     * @return OfferParam
     */
    public function getOfferParam(): OfferParam
    {
        return $this->offerParam;
    }

    /**
     * @param mixed $offerParam
     */
    public function setOfferParam(mixed $offerParam): void
    {
        $this->offerParam[] = $offerParam;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return $this
     */
    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Vendor
     */
    public function getVendor(): Vendor
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     * @return $this
     */
    public function setVendor(Vendor $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrencyName(): ?string
    {
        return $this->currencyName;
    }

    /**
     * @param string $currencyName
     * @return $this
     */
    public function setCurrencyName(string $currencyName): self
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryImportId(): ?int
    {
        return $this->categoryImportId;
    }

    /**
     * @param int|null $categoryImportId
     * @return $this
     */
    public function setCategoryImportId(?int $categoryImportId): self
    {
        $this->categoryImportId = $categoryImportId;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    /**
     * @param bool|null $available
     * @return $this
     */
    public function setAvailable(?bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSellingType(): ?string
    {
        return $this->sellingType;
    }

    /**
     * @param string|null $sellingType
     * @return $this
     */
    public function setSellingType(?string $sellingType): self
    {
        $this->sellingType = $sellingType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return $this
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriceMargin(): ?float
    {
        return $this->priceMargin;
    }

    /**
     * @param float $priceMargin
     * @return $this
     */
    public function setPriceMargin(float $priceMargin): self
    {
        $this->priceMargin = $priceMargin;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    /**
     * @param string|null $vendorCode
     * @return $this
     */
    public function setVendorCode(?string $vendorCode): self
    {
        $this->vendorCode = $vendorCode;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDelivery(): ?bool
    {
        return $this->delivery;
    }

    /**
     * @param bool|null $delivery
     * @return $this
     */
    public function setDelivery(?bool $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    /**
     * @param string|null $barcode
     * @return $this
     */
    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPortalCategoryId(): ?int
    {
        return $this->portalCategoryId;
    }

    /**
     * @param int|null $portalCategoryId
     * @return $this
     */
    public function setPortalCategoryId(?int $portalCategoryId): self
    {
        $this->portalCategoryId = $portalCategoryId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param string|null $keywords
     * @return $this
     */
    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsRecommended(): ?bool
    {
        return $this->isRecommended;
    }

    /**
     * @param bool|null $isRecommended
     */
    public function setIsRecommended(?bool $isRecommended): void
    {
        $this->isRecommended = $isRecommended;
    }

    /**
     * @return bool
     */
    public function getNeedTranslate(): bool
    {
        return $this->needTranslate;
    }

    /**
     * @param bool $needTranslate
     */
    public function setNeedTranslate(bool $needTranslate): void
    {
        $this->needTranslate = $needTranslate;
    }

    /**
     * @return float
     */
    public function getOldPrice(): float
    {
        return $this->oldPrice;
    }

    /**
     * @param float|null $oldPrice
     */
    public function setOldPrice(?float $oldPrice): void
    {
        $this->oldPrice = $oldPrice;
    }

    /**
     * @param DateTimeInterface|string $t_ins
     */
    public function setTIns(DateTimeInterface|string $t_ins): void
    {
        $this->t_ins = $t_ins;
    }

    /**
     * @return DateTimeInterface|string
     */
    public function getTIns(): DateTimeInterface|string
    {
        return $this->t_ins;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->setTIns(new DateTime());
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
