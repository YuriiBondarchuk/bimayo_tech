<?php

namespace App\Entity\site;

use App\Repository\site\CurrenciesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Currencies
 */
#[ORM\Table(name: "site.currencies")]
#[ORM\Entity(repositoryClass: CurrenciesRepository::class)]
class Currencies
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.currencies_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "name", type: "string", length: 155, nullable: false)]
    private string $name;

    #[ORM\Column(name: "rate", type: "float", precision: 10, scale: 0, nullable: false)]
    private float $rate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     * @return $this
     */
    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
