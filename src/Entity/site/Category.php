<?php

namespace App\Entity\site;

use App\Repository\site\CategoryRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 */
#[ORM\Table(name: "site.category")]
#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ORM\UniqueConstraint(name: "category", columns: ["name", "import_id"])]
#[ORM\HasLifecycleCallbacks]
class Category
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.category_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "import_id", type: "integer", nullable: false)]
    private int $importId;

    #[ORM\Column(name: "import_parent_id", type: "integer", nullable: false)]
    private int $importParentId;

    #[ORM\Column(name: "order_number", type: "integer", nullable: true)]
    private int $orderNumber;

    #[ORM\Column(name: "name", type: "string", nullable: false)]
    private string $name;

    #[ORM\Column(name: "url", type: "string", nullable: false)]
    private string $url;

    #[ORM\Column(name: "need_translate", type: "boolean", nullable: false)]
    private bool $needTranslate;

    #[ORM\Column(name: "t_ins", type: "datetime", nullable: false, options: ["default" => "CURRENT_TIMESTAMP(0)"])]
    private string|DateTimeInterface $t_ins;

//    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Offer::class)]
//    private Collection $offers;

//    public function __construct()
//    {
//        $this->offers = new ArrayCollection();
//    }

//    /**
//     * @return Collection
//     */
//    public function getOffers(): Collection
//    {
//        return $this->offers;
//    }
//
//    /**
//     * Add offers
//     *
//     * @param Offer $offer
//     *
//     * @return self
//     */
//    public function addOffer(Offer $offer): self
//    {
//        if (!$this->offers->contains($offer)) {
//            $this->offers[] = $offer;
//            $offer->setCategory($this);
//        }
//
//        return $this;
//    }
//
//    /**
//     * Remove offre
//     *
//     * @param Offer $offer
//     *
//     * @return self
//     */
//    public function removeOffer(Offer $offer): self
//    {
//        if ($this->offers->contains($offer)) {
//            $this->offers->removeElement($offer);
//
//            if ($offer->getCategory() === $this) {
//                $offer->setCategory(null);
//            }
//        }
//
//        return $this;
//    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getImportId(): ?int
    {
        return $this->importId;
    }

    /**
     * @param int $importId
     * @return $this
     */
    public function setImportId(int $importId): self
    {
        $this->importId = $importId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderNumber(): int
    {
        return $this->orderNumber;
    }

    /**
     * @param int $orderNumber
     */
    public function setOrder(int $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return int|null
     */
    public function getImportParentId(): ?int
    {
        return $this->importParentId;
    }

    /**
     * @param int $importParentId
     * @return $this
     */
    public function setImportParentId(int $importParentId): self
    {
        $this->importParentId = $importParentId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return bool
     */
    public function getNeedTranslate(): bool
    {
        return $this->needTranslate;
    }

    /**
     * @param bool $needTranslate
     */
    public function setNeedTranslate(bool $needTranslate): void
    {
        $this->needTranslate = $needTranslate;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @param DateTimeInterface|string $t_ins
     */
    public function setTIns(DateTimeInterface|string $t_ins): void
    {
        $this->t_ins = $t_ins;
    }

    /**
     * @return DateTimeInterface|string
     */
    public function getTIns(): DateTimeInterface|string
    {
        return $this->t_ins;
    }

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->setTIns(new DateTime());
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
