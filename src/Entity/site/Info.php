<?php

namespace App\Entity\site;

use App\Repository\site\InfoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Info
 */
#[ORM\Table(name: "site.info")]
#[ORM\UniqueConstraint(name: "user__email", columns: ["email"])]
#[ORM\Entity(repositoryClass: InfoRepository::class)]
class Info
{
    #[ORM\Column(name: "id", type: "integer", nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName: "site.info_id_seq", allocationSize: 1, initialValue: 1)]
    private int $id;

    #[ORM\Column(name: "email", type: "string", length: 180, nullable: false)]
    private string $email;

    #[ORM\Column(name: "phone", type: "string", length: 180, nullable: true)]
    private string $phone;

    #[ORM\Column(name: "about_us", type: "text", nullable: true)]
    private string $aboutUs;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAboutUs(): string
    {
        return $this->aboutUs;
    }

    /**
     * @param string $aboutUs
     */
    public function setAboutUs(string $aboutUs): void
    {
        $this->aboutUs = $aboutUs;
    }
}
