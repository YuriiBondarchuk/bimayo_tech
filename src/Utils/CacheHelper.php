<?php

declare(strict_types=1);

namespace App\Utils;


use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Helper function related to the caching (primarily internal for the classes).
 */
class CacheHelper
{
    /**
     * Get or set cache with exception handling.
     *
     * @param CacheInterface $cache
     * @param string $key
     * @param callable $callback
     *
     * @return mixed
     */
    public static function getOrSet(CacheInterface $cache, string $key, callable $callback): mixed
    {
        try {
            return $cache->get($key, $callback);
        } catch (InvalidArgumentException $e) {
            throw new RuntimeException("Internal cache error (key = '{$key}').");
        }
    }

    /**
     * Cache key invalidation with exception handling.
     *
     * @param CacheInterface $cache
     * @param string $key
     */
    public static function delete(CacheInterface $cache, string $key): void
    {
        try {
            $cache->delete($key);
        } catch (InvalidArgumentException $e) {
            throw new RuntimeException("Cache invalidation error (key = '{$key}').");
        }
    }
}