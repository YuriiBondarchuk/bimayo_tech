<?php

declare(strict_types=1);

namespace App\Utils;

/**
 * Generic low-level helpers for environment variables, etc.
 */
class SystemHelper
{
    /**
     * Get name of active application.
     *
     * @return string
     */
    public static function getAppName(): string
    {
        return $_SERVER['APP_PROJECT_NAME'];
    }

    /**
     * Get application environment (test, dev or prod).
     *
     * @return string
     */
    public static function getAppEnv(): string
    {
        return $_SERVER['APP_ENV'];
    }

    /**
     * Is application running in production environment?
     *
     * @return bool
     */
    public static function isProdEnv(): bool
    {
        return self::getAppEnv() === 'prod';
    }

    /**
     * Get project domain
     *
     * @return string
     */
    public static function getProjectDomain(): string
    {
        return $_SERVER['APP_PROJECT_DOMAIN'];
    }

    /**
     * Get project base url
     *
     * @return string
     */
    public static function getProjectBaseUrl(): string
    {
        return 'https://' . self::getAppName() . '.' . self::getProjectDomain();
    }

}
